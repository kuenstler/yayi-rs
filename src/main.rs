use std::cell::Cell;
use std::env;

use gio::prelude::*;
use ignore_result::Ignore;

use yayi::{build_backend, build_ui};

/// Not that much
fn main() {
    let application = gtk::Application::new(Some("de.fabi123.yayi"), Default::default())
        .expect("Initialization failed...");

    let (thread, sender_channels, receiver_channels, settings) = build_backend();

    let channels_rc = Cell::new(Some(receiver_channels));

    application.connect_activate(move |application| {
        build_ui(
            application,
            sender_channels.clone(),
            channels_rc.take().unwrap(),
            settings.clone(),
        )
    });

    application.run(&env::args().collect::<Vec<_>>());

    thread.join().ignore();
}
