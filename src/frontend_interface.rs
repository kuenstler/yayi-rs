use async_std::{sync::{Arc, Mutex}, task::block_on};
use gtk::{self, prelude::*};

use crate::current_downloads::frontend::CurrentDownloadFrontend;
use crate::download_list::frontend::DownloadFrontend;
use crate::fl;
use crate::like_list::frontend::LikeFrontend;
use crate::offline_search::frontend::OfflineSearchFrontend;
use crate::progress_bar::Gui as ProgressGui;

use super::WindowObjects;

/// Construct a list with a frame, used by construct_window
pub fn framed_list(name: &str, label: &str) -> (gtk::Frame, gtk::ListBox) {
    let list = gtk::ListBoxBuilder::new()
        .name(&format!("{}{}", name, "List"))
        .activate_on_single_click(true)
        .selection_mode(gtk::SelectionMode::Single)
        .build();
    let list_viewport = gtk::ViewportBuilder::new()
        .name(&format!("{}{}", name, "ListViewport"))
        .child(&list)
        .build();
    let list_scroll = gtk::ScrolledWindowBuilder::new()
        .name(&format!("{}{}", name, "ListScroll"))
        .child(&list_viewport)
        .build();
    let list_frame = gtk::FrameBuilder::new()
        .name(&format!("{}{}", name, "ListFrame"))
        .label(label)
        .child(&list_scroll)
        .build();
    (list_frame, list)
}

/// Images for at least download list popup
pub struct Images {
    pub browser: gtk::Image,
    pub close: gtk::Image,
    pub download: gtk::Image,
    pub download1: gtk::Image,
    pub download_prio: gtk::Image,
    pub not_download: gtk::Image,
    pub watch: gtk::Image,
}

impl Images {
    /// Constructor
    pub fn new() -> Images {
        let browser = gtk::Image::from_icon_name(Some("text-html"), gtk::IconSize::Button);
        let close = gtk::Image::from_icon_name(Some("dialog-close"), gtk::IconSize::Button);
        let download = gtk::Image::from_icon_name(Some("download"), gtk::IconSize::Button);
        let download1 = gtk::Image::from_icon_name(Some("download"), gtk::IconSize::Button);
        let download_prio = gtk::Image::from_icon_name(Some("download"), gtk::IconSize::Button);
        let not_download = gtk::Image::from_icon_name(Some("dialog-cancel"), gtk::IconSize::Button);
        let watch = gtk::Image::from_icon_name(Some("video-x-generic"), gtk::IconSize::Button);
        Images {
            browser,
            close,
            download,
            download1,
            download_prio,
            not_download,
            watch,
        }
    }
}

/// Construct the main window
pub fn construct_window(
    settings: Arc<Mutex<crate::Settings>>,
    sender_channels: super::SenderChannels,
) -> WindowObjects {
    let mut download_list = DownloadFrontend::new(sender_channels.download_tx);
    // Like list
    let mut like_list = LikeFrontend::new(sender_channels.like_tx);
    // Current Download list
    let mut current_download_list = CurrentDownloadFrontend::new(sender_channels.current_tx);
    // Download Box
    let download_box = gtk::BoxBuilder::new()
        .orientation(gtk::Orientation::Vertical)
        .spacing(1)
        .name("DownloadBox")
        .homogeneous(true)
        .build();
    download_list.add_to_widget(&download_box).unwrap();
    like_list.add_to_widget(&download_box).unwrap();
    current_download_list.add_to_widget(&download_box).unwrap();
    // Stream list
    let (stream_list_frame, stream_list) = framed_list("Stream", &fl!("stream-list-label"));
    let mut offline_search = OfflineSearchFrontend::new(sender_channels.offline_search_tx);
    // The tabs
    let tabs = gtk::NotebookBuilder::new().name("Tabs").build();
    let downloads_label = gtk::LabelBuilder::new()
        .name("DownloadsLabel")
        .label(&fl!("downloads-tabs-label"))
        .tooltip_text(&fl!("downloads-tabs-tooltip"))
        .build();
    tabs.append_page(&download_box, Some(&downloads_label));
    let streams_label = gtk::LabelBuilder::new()
        .name("StreamsLabel")
        .label(&fl!("streams-tabs-label"))
        .tooltip_text(&fl!("streams-tabs-tooltip"))
        .build();
    tabs.append_page(&stream_list_frame, Some(&streams_label));
    offline_search.add_to_widget(&tabs).unwrap();
    // footer progress bar
    let mut progress = ProgressGui::new(sender_channels.backend_download_tx);
    // footer and main view separation
    let separator = gtk::BoxBuilder::new()
        .name("Separator")
        .orientation(gtk::Orientation::Vertical)
        .spacing(1)
        .build();
    separator.pack_start(&tabs, true, true, 0);
    progress.add_to_widget(&separator).unwrap();
    // menu bar
    let header = gtk::HeaderBarBuilder::new()
        .name("Header")
        .title("YAYI")
        .subtitle(&fl!("window-subtitle"))
        .spacing(1)
        .show_close_button(true)
        .build();
    let settings_ref = block_on(settings.lock());
    // The window
    let window = gtk::ApplicationWindowBuilder::new()
        .name("Window")
        .child(&separator)
        .window_position(gtk::WindowPosition::None)
        .default_width(settings_ref.window.width)
        .default_height(settings_ref.window.height)
        .build();
    window.set_titlebar(Some(&header));
    drop(settings_ref);
    // Populate structs
    WindowObjects {
        window,
        progress,
        download_list,
        like_list,
        current_download_list,
        stream_list,
        offline_search,
    }
}
