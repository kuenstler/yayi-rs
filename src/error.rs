use std::error;
use std::fmt;


use crate::fl;

#[derive(Debug)]
pub enum Error {
    /// State machine had wrong state for called method
    /// expects a name, the expected and finally the received state
    WrongState(
        &'static str,
        crate::common_structs::Status,
        crate::common_structs::Status,
    ),
    /// For youtube interaction, msg
    YoutubeMissing(String),
    /// Rustube Error and vid
    RustubeError(rustube::Error, String),
    WrongDatetime,
    NoVideoStreamFound(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            Error::WrongState(name, expected, received) => {
                let name = *name;
                let expected = expected.to_string();
                let received = received.to_string();
                let message: String = fl!(
                    "internal-error-wrong-state",
                    name = name,
                    expected = expected,
                    received = received
                );
                write!(f, "{}", &message)
            }
            Error::YoutubeMissing(msg) => {
                let msg = msg.clone();
                let message: String = fl!("internal-error-youtube-missing", msg = msg);
                write!(f, "{}", &message)
            }
            Error::RustubeError(err, vid) => {
                let err = format!("{:?}", err);
                let vid = vid.clone();
                let message: String = fl!("rustube-error", err = err, vid = vid);
                write!(f, "{}", message)
            }
            Error::WrongDatetime => {
                write!(f, "Wrong Datetime")
            }
            Error::NoVideoStreamFound(vid) => {
                let vid = vid.clone();
                let message: String = fl!("no-video-stream-found", vid = vid);
                write!(f, "{}", message)
            }
        }
    }
}

impl error::Error for Error {}

fn _assert_error_is_sync_send() {
    fn _is_sync_send<T: Sync + Send>() {}
    _is_sync_send::<Error>();
}
