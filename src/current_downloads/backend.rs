use std::error::Error;

use async_std::channel::{Receiver as AReceiver, Sender as ASender, unbounded};
use futures::StreamExt;
use sqlx::{self, SqlitePool};

use crate::backend::ControlFlow;
use crate::common_structs::AddButton;

use super::frontend::*;

type DownloadTx = async_std::channel::Sender<crate::download_list::backend::DownloadListAction>;

/// An event that gets emitted by a download list popup
#[derive(Clone)]
pub enum ListAction {
    // vid
    AddEntry(String),
    RemoveEntry(String),
    MoveUp(String),
    MoveTop(String),
    MoveDown(String),
    MoveBottom(String),
    Cancel(String),
    ControlFlow(ControlFlow),
}

/// Communication channels for continuous
pub struct Communication {
    pub to_gui: glib::Sender<ListChange>,
    pub from_gui: AReceiver<ListAction>,
}

impl Communication {
    pub fn new() -> (
        glib::Receiver<ListChange>,
        ASender<ListAction>,
        Communication,
    ) {
        let (list_change_tx, list_change_rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let (list_action_tx, list_action_rx) = unbounded();
        let communication = Communication {
            to_gui: list_change_tx,
            from_gui: list_action_rx,
        };
        (list_change_rx, list_action_tx, communication)
    }
}

/// Fill the download list with initial entries
pub async fn init(
    to_gui: glib::Sender<ListChange>,
    pool: &SqlitePool,
) -> Result<(), Box<dyn Error>> {
    let results: Vec<AddButton> = sqlx::query_file_as!(
        AddButton,
        "src/current_downloads/queries/select_list_videos.sql"
    )
        .fetch_all(pool)
        .await?;

    for result in results {
        to_gui.send(ListChange::AddButton(result))?;
    }

    Ok(())
}

async fn cancel_download(
    vid: String,
    pool: &SqlitePool,
    to_gui: &glib::Sender<ListChange>,
    download_tx: &DownloadTx,
) -> Result<(), Box<dyn Error>> {
    let mut transaction = pool.begin().await?;
    sqlx::query_file!(
        "src/current_downloads/queries/delete_cancel_download.sql",
        vid
    )
        .fetch_optional(&mut transaction)
        .await?;
    sqlx::query_file!(
        "src/current_downloads/queries/update_download_status.sql",
        vid
    )
        .fetch_optional(&mut transaction)
        .await?;
    transaction.commit().await?;
    to_gui.send(ListChange::RemoveButton(vid.clone()))?;
    download_tx
        .send(crate::download_list::backend::DownloadListAction::AddEntry(
            vid,
        ))
        .await?;

    Ok(())
}

struct Id {
    pub id: i64,
}

enum Direction {
    Up,
    Top,
    Down,
    Bottom,
}

async fn change_pos(
    vid: String,
    pool: &SqlitePool,
    to_gui: &glib::Sender<ListChange>,
    direction: Direction,
) -> Result<(), Box<dyn Error>> {
    let mut transaction = pool.begin().await?;
    let ids: Vec<Id> = match direction {
        Direction::Top => {
            sqlx::query_file_as!(Id, "src/current_downloads/queries/select_first_id.sql")
                .fetch_all(&mut transaction)
                .await?
        }
        Direction::Up => {
            sqlx::query_file_as!(Id, "src/current_downloads/queries/select_prior_2.sql", vid)
                .fetch_all(&mut transaction)
                .await?
        }
        Direction::Bottom => {
            sqlx::query_file_as!(Id, "src/current_downloads/queries/select_last_id.sql")
                .fetch_all(&mut transaction)
                .await?
        }
        Direction::Down => {
            sqlx::query_file_as!(Id, "src/current_downloads/queries/select_next_2.sql", vid)
                .fetch_all(&mut transaction)
                .await?
        }
    };
    if !ids.is_empty() {
        let first = ids
            .get(1)
            .map(|idx| Result::<i64, Box<dyn Error>>::Ok(idx.id))
            .unwrap_or_else(|| match direction {
                Direction::Up | Direction::Top => Ok(0),
                Direction::Down | Direction::Bottom => {
                    let result: Id = async_std::task::block_on(
                        sqlx::query_file_as!(
                            Id,
                            "src/current_downloads/queries/select_last_id.sql"
                        )
                            .fetch_one(&mut transaction),
                    )?;
                    Ok(result.id + 100)
                }
            })?;
        let second = ids[0].id;
        let new_pos = (first + second) / 2;
        if new_pos != first && new_pos != second {
            sqlx::query_file!(
                "src/current_downloads/queries/update_download_list_pos.sql",
                new_pos,
                vid
            )
                .fetch_optional(&mut transaction)
                .await?;
            transaction.commit().await?;
            let event = match direction {
                Direction::Top => ListChange::MoveTop(vid),
                Direction::Up => ListChange::MoveUp(vid),
                Direction::Down => ListChange::MoveDown(vid),
                Direction::Bottom => ListChange::MoveBottom(vid),
            };
            to_gui.send(event)?;
        }
    }
    Ok(())
}

pub async fn add_entry(
    to_gui: &glib::Sender<ListChange>,
    pool: &SqlitePool,
    vid: String,
) -> Result<(), Box<dyn Error>> {
    let result: AddButton = sqlx::query_file_as!(
        AddButton,
        "src/current_downloads/queries/select_list_video_by_id.sql",
        vid
    )
        .fetch_one(pool)
        .await?;

    to_gui.send(ListChange::AddButton(result))?;

    Ok(())
}

fn remove_entry(to_gui: &glib::Sender<ListChange>, vid: String) -> Result<(), Box<dyn Error>> {
    to_gui.send(ListChange::RemoveButton(vid))?;

    Ok(())
}

pub async fn continuous(
    mut communication: Communication,
    download_tx: DownloadTx,
    pool: sqlx::SqlitePool,
) -> Result<(), Box<dyn Error>> {
    let to_gui = communication.to_gui;
    while let Some(msg) = communication.from_gui.next().await {
        match msg {
            ListAction::Cancel(vid) => {
                async_std::task::block_on(cancel_download(vid, &pool, &to_gui, &download_tx))?
            }
            ListAction::MoveUp(vid) => {
                async_std::task::block_on(change_pos(vid, &pool, &to_gui, Direction::Up))?
            }
            ListAction::MoveTop(vid) => {
                async_std::task::block_on(change_pos(vid, &pool, &to_gui, Direction::Top))?
            }
            ListAction::MoveDown(vid) => {
                async_std::task::block_on(change_pos(vid, &pool, &to_gui, Direction::Down))?
            }
            ListAction::MoveBottom(vid) => {
                async_std::task::block_on(change_pos(vid, &pool, &to_gui, Direction::Bottom))?
            }
            ListAction::AddEntry(vid) => async_std::task::block_on(add_entry(&to_gui, &pool, vid))?,
            ListAction::RemoveEntry(vid) => remove_entry(&to_gui, vid)?,
            ListAction::ControlFlow(flow) => match flow {
                ControlFlow::Quit => break,
            },
        }
    }
    Ok(())
}
