//! These methods should be executed within the main GUI thread and do no blocking work.
//! They are just spawning and handling windows and send the blocking work in form of
//! messages to the backend using appropriate channels

use gtk::prelude::*;
use ignore_result::Ignore;

use crate::common_structs::{AddButton, Status};
use crate::fl;
use crate::frontend_interface::framed_list;

use super::backend::ListAction;

/// Events for the download list
pub enum ListChange {
    AddButton(AddButton),
    RemoveButton(String),
    MoveUp(String),
    MoveTop(String),
    MoveDown(String),
    MoveBottom(String),
}

#[derive(Clone)]
pub struct CurrentDownloadFrontend {
    top: gtk::Button,
    up: gtk::Button,
    down: gtk::Button,
    bottom: gtk::Button,
    cancel: gtk::Button,
    list: gtk::ListBox,
    frame: gtk::Frame,
    status: Status,
    to_backend: async_std::channel::Sender<ListAction>,
    //vids:  Option<Arc<RwLock<HashMap<gtk::ListBoxRow, String>>>>,
    //positions: Option<Arc<RwLock<HashMap<gtk::ListBoxRow, Mutex<usize>>>>>,
}

impl CurrentDownloadFrontend {
    pub fn new(to_backend: async_std::channel::Sender<ListAction>) -> Self {
        let (frame, list) = framed_list("CurrentDownload", &fl!("current-download-list-label"));
        let top_image = gtk::ImageBuilder::new().icon_name("go-top").build();
        let top_button = gtk::ButtonBuilder::new()
            .image(&top_image)
            .tooltip_text(&fl!("current-download-top-tooltip"))
            .build();
        let up_image = gtk::ImageBuilder::new().icon_name("go-up").build();
        let up_button = gtk::ButtonBuilder::new()
            .image(&up_image)
            .tooltip_text(&fl!("current-download-up-tooltip"))
            .build();
        let down_image = gtk::ImageBuilder::new().icon_name("go-down").build();
        let down_button = gtk::ButtonBuilder::new()
            .image(&down_image)
            .tooltip_text(&fl!("current-download-down-tooltip"))
            .build();
        let bottom_image = gtk::ImageBuilder::new().icon_name("go-bottom").build();
        let bottom_button = gtk::ButtonBuilder::new()
            .image(&bottom_image)
            .tooltip_text(&fl!("current-download-bottom-tooltip"))
            .build();
        let button_box = gtk::ButtonBoxBuilder::new()
            .orientation(gtk::Orientation::Vertical)
            .homogeneous(true)
            .layout_style(gtk::ButtonBoxStyle::Expand)
            .build();
        button_box.add(&top_button);
        button_box.add(&up_button);
        button_box.add(&down_button);
        button_box.add(&bottom_button);
        let cancel_image = gtk::ImageBuilder::new().icon_name("dialog-close").build();
        let cancel_button = gtk::ButtonBuilder::new()
            .image(&cancel_image)
            .tooltip_text(&fl!("current-download-cancel-tooltip"))
            .build();
        let right_button_box = gtk::ButtonBoxBuilder::new()
            .orientation(gtk::Orientation::Vertical)
            .homogeneous(true)
            .layout_style(gtk::ButtonBoxStyle::Expand)
            .build();
        right_button_box.add(&cancel_button);
        let greater_box = gtk::BoxBuilder::new().build();
        let list_scroll = frame.get_child().unwrap();
        frame.remove(&list_scroll);
        greater_box.pack_start(&list_scroll, true, true, 0);
        greater_box.pack_start(&button_box, false, true, 0);
        greater_box.pack_start(&right_button_box, false, true, 0);
        frame.add(&greater_box);
        CurrentDownloadFrontend {
            top: top_button,
            up: up_button,
            down: down_button,
            bottom: bottom_button,
            cancel: cancel_button,
            list,
            frame,
            status: Default::default(),
            to_backend,
            //vids: Some(Arc::new(RwLock::new(HashMap::new()))),
            //positions: Some(Arc::new(RwLock::new(HashMap::new()))),
        }
    }

    pub fn add_to_widget<W: IsA<gtk::Container>>(
        &mut self,
        parent: &W,
    ) -> Result<(), crate::Error> {
        if let Status::Constructed = self.status {
            parent.add(&self.frame);
            self.status = Status::Added;
            Ok(())
        } else {
            Err(crate::Error::WrongState(
                "CurrentDownloadsFrontend",
                Status::Constructed,
                self.status,
            ))
        }
    }

    /// Connect events for current_list
    pub fn connect_events(&mut self) -> Result<(), crate::Error> {
        if let Status::Added = self.status {
            // Top button
            let list_clone = self.list.clone();
            let to_backend_clone = self.to_backend.clone();

            self.top.connect_clicked(move |_| {
                if let Some(child) = list_clone.get_selected_row() {
                    let name = child.get_widget_name();
                    let id = name[0..name.len() - 18].to_string();
                    to_backend_clone.try_send(ListAction::MoveTop(id)).ignore();
                }
            });

            // Up button
            let list_clone = self.list.clone();
            let to_backend_clone = self.to_backend.clone();

            self.up.connect_clicked(move |_| {
                if let Some(child) = list_clone.get_selected_row() {
                    let name = child.get_widget_name();
                    let id = name[0..name.len() - 18].to_string();
                    to_backend_clone.try_send(ListAction::MoveUp(id)).ignore();
                }
            });

            // Down button
            let list_clone = self.list.clone();
            let to_backend_clone = self.to_backend.clone();

            self.down.connect_clicked(move |_| {
                if let Some(child) = list_clone.get_selected_row() {
                    let name = child.get_widget_name();
                    let id = name[0..name.len() - 18].to_string();
                    to_backend_clone.try_send(ListAction::MoveDown(id)).ignore();
                }
            });

            // Bottom button
            let list_clone = self.list.clone();
            let to_backend_clone = self.to_backend.clone();

            self.bottom.connect_clicked(move |_| {
                if let Some(child) = list_clone.get_selected_row() {
                    let name = child.get_widget_name();
                    let id = name[0..name.len() - 18].to_string();
                    to_backend_clone
                        .try_send(ListAction::MoveBottom(id))
                        .ignore();
                }
            });

            // Cancel button
            let list_clone = self.list.clone();
            let to_backend_clone = self.to_backend.clone();

            self.cancel.connect_clicked(move |_| {
                if let Some(child) = list_clone.get_selected_row() {
                    let name = child.get_widget_name();
                    let id = name[0..name.len() - 18].to_string();
                    to_backend_clone.try_send(ListAction::Cancel(id)).ignore();
                }
            });

            self.status = Status::Connected;
            Ok(())
        } else {
            Err(crate::Error::WrongState(
                "CurrentDownloadsFrontend",
                Status::Added,
                self.status,
            ))
        }
    }

    fn add_button(&self, add_btn: AddButton) {
        let title_label = gtk::LabelBuilder::new()
            .label(&fl!("current-download-list-title", title = add_btn.title))
            .tooltip_text(&fl!("current-download-title-tooltip"))
            .build();
        let list_row = gtk::ListBoxRowBuilder::new()
            .name(&format!("{}CurrentDownloadRow", add_btn.vid))
            .child(&title_label)
            .build();
        self.list.add(&list_row);
    }

    fn index_from_vid(&self, vid: String) -> Option<usize> {
        let name = format!("{}CurrentDownloadRow", vid);
        self.list
            .get_children()
            .iter()
            .enumerate()
            .filter(move |(_, w)| w.get_widget_name() == name)
            .map(|(i, _)| i)
            .next()
    }

    fn handle_event(&self, event: ListChange) {
        match event {
            ListChange::AddButton(add_btn) => self.add_button(add_btn),
            ListChange::RemoveButton(vid) => {
                let name = format!("{}CurrentDownloadRow", vid);
                self.list
                    .get_children()
                    .iter()
                    .filter(move |w| w.get_widget_name() == name)
                    .for_each(|w| self.list.remove(w));
            }
            ListChange::MoveUp(vid) => {
                let index = self.index_from_vid(vid);
                if let Some(index) = index {
                    if index > 0 {
                        let row = self.list.get_children()[index].clone();
                        self.list.remove(&row);
                        self.list.insert(&row, (index - 1) as i32);
                    }
                }
            }
            ListChange::MoveTop(vid) => {
                let index = self.index_from_vid(vid);
                if let Some(index) = index {
                    if index > 0 {
                        let row = self.list.get_children()[index].clone();
                        self.list.remove(&row);
                        self.list.insert(&row, 0);
                    }
                }
            }
            ListChange::MoveDown(vid) => {
                let index = self.index_from_vid(vid);
                let length = self.list.get_children().len();
                if let Some(index) = index {
                    if index < length {
                        let row = self.list.get_children()[index].clone();
                        self.list.remove(&row);
                        self.list.insert(&row, (index + 1) as i32);
                    }
                }
            }
            ListChange::MoveBottom(vid) => {
                let index = self.index_from_vid(vid);
                let length = self.list.get_children().len();
                if let Some(index) = index {
                    if index < length {
                        let row = self.list.get_children()[index].clone();
                        self.list.remove(&row);
                        self.list.add(&row);
                    }
                }
            }
        }
        self.list.show_all();
    }

    pub fn attach_handler(
        &mut self,
        action_rx: glib::Receiver<ListChange>,
    ) -> Result<(), crate::Error> {
        if let Status::Connected = self.status {
            let self_clone = self.clone();

            action_rx.attach(None, move |msg| {
                self_clone.handle_event(msg);
                glib::Continue(true)
            });
            self.status = Status::Attached;
            Ok(())
        } else {
            Err(crate::Error::WrongState(
                "CurrentDownloadsFrontend",
                Status::Connected,
                self.status,
            ))
        }
    }

    pub fn check(&self) -> Result<(), crate::Error> {
        if let Status::Attached = self.status {
            Ok(())
        } else {
            Err(crate::Error::WrongState(
                "CurrentDownloadsFrontend",
                Status::Attached,
                self.status,
            ))
        }
    }
}
