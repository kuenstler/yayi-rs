SELECT id
FROM download_list
WHERE id > (
    SELECT id
    FROM download_list
    WHERE vid = ?
)
ORDER BY id ASC
LIMIT 2