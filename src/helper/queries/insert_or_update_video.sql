INSERT INTO videos
(vid, url, title, description, cid, likes, dislikes, views, duration, comments, thumbnail_url, download_status, published)
VALUES (?, ?, ?, ?, 'yt:channel:' || ?, ?, ?, ?, ?, ?, ?, ?, ?)
ON CONFLICT (vid)
DO UPDATE SET
    title=excluded.title,
    description=excluded.description,
    likes=excluded.likes,
    dislikes=excluded.dislikes,
    views=excluded.views,
    comments=excluded.comments;
