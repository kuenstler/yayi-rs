use std::{error::Error, time::Duration};

use async_std::{
    channel::{Receiver, Sender},
    fs,
    future::timeout,
    io,
    path::{Path, PathBuf},
    prelude::*,
    sync::{Arc, Mutex},
};
use tokio::sync::mpsc;
use futures::StreamExt;
use hyper::body::Buf;
use ignore_result::Ignore;
use rustube::{Id, Video, Callback};

use crate::current_downloads::backend::ListAction;
use crate::download_list::backend::DownloadListAction;
use crate::progress_bar::Event as ProgressEvent;

pub enum DownloadSignal {
    Add,
    /// Currently not possible with rustube, will get ignored
    CancelCurrent,
    Quit,
}

struct Vid {
    pub vid: String,
    pub title: String,
    pub description: String,
    pub thumbnail_url: Option<String>,
}

async fn write_thumbnail(vid: &Vid, folder: &PathBuf, name: &str) -> Result<(), io::Error> {
    if let Some(ref tmb_url) = vid.thumbnail_url {
        let tmb_file = folder.join(&format!("{}.jpg", name));
        let tmb_data = reqwest::get(tmb_url).await;
        if let Ok(response) = tmb_data {
            let mut file = fs::File::create(tmb_file).await?;
            let mut response_stream = response.bytes_stream();
            while let Some(chunk) = response_stream.next().await {
                match chunk {
                    Ok(chunk) => file.write_all(chunk.chunk()).await.ignore(),
                    Err(_) => break
                }
            }
        }
    }
    Ok(())
}

async fn write_description(vid: &Vid, folder: &PathBuf, name: &str) -> Result<(), io::Error> {
    let desc_file = folder.join(&format!("{}.description", name));
    let mut file = fs::File::create(desc_file).await?;
    file.write_all(vid.description.as_bytes()).await?;
    Ok(())
}

async fn write_video(
    vid_strip: &&str,
    folder: &PathBuf,
    name: &str,
    vid: &Vid,
    pool: sqlx::SqlitePool,
    current_list_tx: &Sender<ListAction>,
    download_list_tx: &Sender<DownloadListAction>,
    progress_tx: &glib::Sender<ProgressEvent>,
    download_rx: &Receiver<DownloadSignal>,
) -> Result<PathBuf, Box<dyn Error>> {
    let id = Id::from_str(vid_strip)?;
    let video = Video::from_id(id.as_owned()).await?;
    let video_path = folder.join(&format!("{}.mp4", name));
    let selected = video.best_quality();
    if let Some(stream) = selected {
        current_list_tx.send(ListAction::RemoveEntry(vid.vid.clone())).await?;
        sqlx::query_file!("src/helper/queries/update_set_downloading.sql", vid.vid ,vid.vid)
            .execute(&pool)
            .await?;
        progress_tx.send(ProgressEvent::OnProgress(0.0)).unwrap();
        progress_tx.send(ProgressEvent::SetName(name.to_string())).unwrap();
        let length = stream.content_length().await? as f64;
        // Low count due to buffer of 100 eventis within rustube
        let (tx, mut rx) = mpsc::channel(5);
        let callback = Callback::new().connect_on_progress_sender(tx, true);
        // Add callback in the future
        let update_fut = async {
            while let Some(value) = rx.recv().await {
                let original_value = (value.current_chunk as f64) / length;
                // Rethinking the Progress Bar
                // Just feels better
                let better_value = (original_value + (1.0 - original_value)/2.0).powf(8.0);
                progress_tx.send(
                    ProgressEvent::OnProgress(better_value)
                ).ignore();
                if let Ok(event) = download_rx.try_recv() {
                    match event {
                        DownloadSignal::Add => {},
                        _ => {
                            rx.close();
                            break;
                        }
                    }
                }
            }
        };
        // stuff on progress is requested
        let download_fut = stream.download_to_callback(&video_path, callback);
        let (_, download) = futures::future::join(update_fut, download_fut).await;
        match download {
            Ok(_) => {
                progress_tx.send(ProgressEvent::OnProgress(1.0)).unwrap();
                sqlx::query_file!(
                    "src/helper/queries/update_set_downloaded.sql",
                    vid.vid
                )
                    .execute(&pool)
                    .await?;
                Ok(video_path)
            }
            Err(error) => {
                sqlx::query_file!(
                    "src/helper/queries/update_set_to_download.sql",
                    vid.vid
                )
                    .execute(&pool)
                    .await?;
                download_list_tx
                    .send(DownloadListAction::AddEntry(vid.vid.clone()))
                    .await?;
                Err(crate::Error::RustubeError(error, vid_strip.to_string()).into())
            }
        }
    } else {
        Err(crate::Error::NoVideoStreamFound(vid_strip.to_string()).into())
    }
}

pub async fn download(
    download_rx: Receiver<DownloadSignal>,
    current_list_tx: Sender<ListAction>,
    download_list_tx: Sender<DownloadListAction>,
    progress_tx: &glib::Sender<ProgressEvent>,
    error_reporting_tx: &glib::Sender<crate::backend::BackendError>,
    settings: Arc<Mutex<crate::settings::Settings>>,
    pool: &sqlx::SqlitePool,
) -> Result<(), Box<dyn Error>> {
    loop {
        let pool = pool.clone();
        let video: Option<Vid> = sqlx::query_file_as!(Vid, "src/helper/queries/select_download_video.sql")
            .fetch_optional(&pool)
            .await?;
        if let Some(vid) = video {
            let vid_strip = vid.vid.split(':').nth(2).unwrap();
            let name = format!("{} - {}", vid.title.replace("/", "_"), vid_strip);
            let settings_ref = settings.lock().await;
            let parent_folder_name = shellexpand::full(&settings_ref.video_location)?.into_owned();
            drop(settings_ref);
            let mut folder = fs::canonicalize(Path::new(&parent_folder_name)).await?;
            folder.push(&name);
            fs::create_dir_all(&folder).await?;
            write_thumbnail(&vid, &folder, &name).await.ignore();
            write_description(&vid, &folder, &name).await.ignore();
            let res = write_video(&vid_strip, &folder, &name, &vid, pool.clone(), &current_list_tx, &download_list_tx, progress_tx, &download_rx).await;
            if let Err(error) = res {
                if let Some(&crate::Error::RustubeError(rustube::Error::ChannelClosed, _)) =
                    error.downcast_ref::<crate::Error>() {}
                else {
                    error_reporting_tx.send(
                        crate::backend::BackendError::Error(format!("{:?}", error))
                    ).ignore()
                }
            }
        }
        // just 1 database select if nothing changed, so no big deal
        // will also trigger retrys of failed downloads
        let video: Option<Vid> = sqlx::query_file_as!(Vid, "src/helper/queries/select_download_video.sql")
            .fetch_optional(&pool)
            .await?;
        if video.is_some() {
            if let Ok(DownloadSignal::Quit) = download_rx.try_recv() {
                 break
            }
        } else {
            let result = timeout(Duration::from_secs(60), download_rx.recv()).await;
            if let Ok(res) = result {
                // Either add or cancel lead to next iteration at this point
                if let DownloadSignal::Quit = res? { break }
            }
        }
    }
    Ok(())
}
