use std::error::Error;
use std::time::Duration;

use async_std::{
    channel::{Receiver, Sender},
    future::timeout,
    sync::{Arc, Mutex},
};
use atom_syndication::Feed;
use futures::{Future, stream::{FuturesUnordered, StreamExt}};
use rustube::{Id, VideoFetcher, Error as RError};
use chrono::{Local, NaiveTime, TimeZone};

use crate::current_downloads::backend::ListAction;
use crate::download_list::backend::DownloadListAction;
use crate::error;
use crate::fl;
use crate::like_list::backend::LikeListAction;

struct Cid {
    cid: String,
    instant: bool,
}

fn sync_videos(
    iterator: Vec<atom_syndication::Entry>,
    download_tx: &Sender<DownloadListAction>,
    like_tx: &Sender<LikeListAction>,
    current_tx: &Sender<ListAction>,
    pool: &sqlx::SqlitePool,
    instant: bool,
) -> FuturesUnordered<impl Future<Output=Result<(), Box<dyn Error>>>> {
    let download_tx_clone = download_tx.clone();
    let like_tx_clone = like_tx.clone();
    let current_tx_clone = current_tx.clone();
    let pool_clone = pool.clone();
    iterator.into_iter().map(move |e| {
        let download_tx_clone_clone = download_tx_clone.clone();
        let like_tx_clone_clone = like_tx_clone.clone();
        let current_tx_clone_clone = current_tx_clone.clone();
        let pool_clone_clone = pool_clone.clone();
        async move {
            let media_group = e
                .extensions
                .get("media")
                .and_then(|media| media.get("group"))
                .and_then(|group| group.first())
                .map(|group| group.children.clone())
                .ok_or_else(|| crate::Error::YoutubeMissing(fl!("error-missing-media-group")))?;
            // correct formatting enforced by database constraint
            let vid_strip = e.id.split(':').nth(2).unwrap();
            let id = Id::from_str(vid_strip)?.into_owned();
            let exists = sqlx::query_file!("src/helper/queries/select_vid.sql", e.id)
                .fetch_optional(&pool_clone_clone)
                .await?;
            if exists.is_none() {
                let video_task = VideoFetcher::from_id(id.clone())?;
                let video_info = match video_task.fetch().await {
                    Ok(video) => {
                        video.descramble()?.video_info().clone()
                    }
                    Err(err) => {
                        match err {
                            RError::VideoUnavailable(_) => {
                                let video_task = VideoFetcher::from_id(id.clone())?;
                                video_task.fetch_info().await?
                            }
                            _ => {
                                let err = error::Error::RustubeError(err, vid_strip.to_string());
                                return Err::<(), Box<dyn Error>>(err.into());
                            }
                        }
                    }
                };
                let video_details = video_info.player_response.video_details;
                if !video_details.is_live_content {
                    let url = id.watch_url().to_string();
                    let thumbnail_url = video_details.thumbnails.first().map(|t| t.url.clone());
                    let download_status: i64 = if instant { 0 } else { 3 };
                    let views = video_details.view_count as i64;
                    let duration = video_details.length_seconds as i64;
                    let (likes, dislikes) = if let Some(value) = {
                        media_group
                            .get("community")
                            .and_then(|comm| comm.first())
                            .map(|ext| &ext.children)
                            .and_then(|map| map.get("starRating"))
                            .and_then(|star| star.first())
                            .map(|star| &star.attrs)
                            .and_then(|star| {
                                if let (Some(count), Some(average)) = (
                                    star.get("count").and_then(|c| c.parse::<i64>().ok()),
                                    star.get("average").and_then(|a| a.parse::<f64>().ok())
                                ) {
                                    Some((count, average))
                                } else {
                                    None
                                }
                            })
                    } {
                        let likes = (value.0 as f64 * value.1) as i64;
                        (likes, value.0 - likes)
                    } else {
                        let likes = (1000.0 * video_details.average_rating) as i64;
                        (likes, 1000 - likes)
                    };
                    let date = Local.from_local_datetime(
                        &video_info
                            .player_response
                            .microformat
                            .player_microformat_renderer
                            .publish_date
                            .and_time(NaiveTime::from_hms(0, 0, 0))
                    )
                        .earliest()
                        .ok_or(crate::Error::WrongDatetime)?
                        .to_rfc3339();
                    let description = media_group
                        .get("description")
                        .and_then(|desc| desc.first())
                        .and_then(|desc| desc.value.clone())
                        .unwrap_or_else(|| video_details.short_description.clone());
                    sqlx::query_file!(
                        "src/helper/queries/insert_or_update_video.sql",
                        e.id,
                        url,
                        video_details.title,
                        description,
                        video_details.channel_id,
                        likes,
                        dislikes,
                        views,
                        duration,
                        0,
                        thumbnail_url,
                        download_status,
                        date
                    )
                        .fetch_optional(&pool_clone_clone)
                        .await?;
                    if instant {
                        current_tx_clone_clone.send(ListAction::AddEntry(e.id.to_string())).await?;
                    } else {
                        download_tx_clone_clone.send(DownloadListAction::AddEntry(e.id.to_string())).await?;
                        like_tx_clone_clone.send(LikeListAction::AddEntry(e.id.to_string())).await?;
                    }
                }
            }
            Ok::<(), Box<dyn Error>>(())
        }
    }).collect()
}

pub async fn sync(
    settings: Arc<Mutex<crate::settings::Settings>>,
    download_tx: Sender<DownloadListAction>,
    like_tx: Sender<LikeListAction>,
    current_tx: Sender<ListAction>,
    control_rx: Receiver<super::Control>,
    pool: &sqlx::SqlitePool,
) -> Result<(), Box<dyn Error>> {
    let pool = pool.clone();
    loop {
        let channels: Vec<Cid> = sqlx::query_file_as!(Cid, "src/helper/queries/select_cids.sql")
            .fetch_all(&pool)
            .await?;
        for channel in channels {
            // correct format forced by database constraint
            let cid_strip = channel.cid.split(':').nth(2).unwrap();
            let response = reqwest::get(format!("https://www.youtube.com/feeds/videos.xml?channel_id={}", cid_strip)).await;
            if let Ok(response) = response {
                if let Ok(feed_text) = response.text().await {
                    let feed = feed_text.parse::<Feed>();
                    if let Ok(feed) = feed {
                        let mut futures = sync_videos(feed.entries, &download_tx, &like_tx, &current_tx, &pool, channel.instant);
                        while let Some(fut) = futures.next().await {
                            if let Err(err) = fut {
                                if let Some(source) = err.source() {
                                    println!("source: {:?}", source)
                                } else {
                                    println!("{:?}", err);
                                }
                            }
                        }
                    }
                }
            }
        }
        let settings_ref = settings.lock().await;
        let duration = Duration::from_secs(settings_ref.fetch_interval);
        drop(settings_ref);
        let result = timeout(duration, control_rx.recv()).await;
        if let Ok(res) = result {
            match res? {
                super::Control::Exit => break
            }
        }
    }
    Ok(())
}
