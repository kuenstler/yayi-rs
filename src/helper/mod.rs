use std::error::Error;

use async_std::{channel::{Receiver, Sender}, sync::{Arc, Mutex}};
//use google_youtube3::YouTube as gYoutube;
//use hyper::{Client as hClient, client::HttpConnector};
//use hyper_rustls::HttpsConnector;
//use yup_oauth2::authenticator::DefaultAuthenticator;

use crate::current_downloads::backend::ListAction;
use crate::download_list::backend::DownloadListAction;
use crate::like_list::backend::LikeListAction;
use crate::progress_bar::Event as ProgressEvent;

//type Client = hClient<HttpsConnector<HttpConnector>>;
//type YouTube = gYoutube<Client, DefaultAuthenticator>;

pub mod sync;
pub mod download;

pub enum Control {
    Exit
}

pub async fn run(
    pool: sqlx::SqlitePool,
    control_rx: Receiver<Control>,
    download_rx: Receiver<download::DownloadSignal>,
    download_tx: Sender<download::DownloadSignal>,
    current_list_tx: Sender<ListAction>,
    download_list_tx: Sender<DownloadListAction>,
    like_list_tx: Sender<LikeListAction>,
    progress_tx: &glib::Sender<ProgressEvent>,
    error_reporting_tx: &glib::Sender<crate::backend::BackendError>,
    settings: Arc<Mutex<crate::settings::Settings>>,
) -> Result<(), Box<dyn Error>> {
    let download_fut = download::download(
        download_rx,
        current_list_tx.clone(),
        download_list_tx.clone(),
        progress_tx,
        error_reporting_tx,
        settings.clone(),
        &pool,
    );
    let sync_fut = sync::sync(
        settings,
        download_list_tx,
        like_list_tx,
        current_list_tx,
        control_rx.clone(),
        &pool,
    );
    let channel_fut = async move {
        while let Ok(msg) = control_rx.recv().await {
            // Currently only 1 Control signal exists
            #[allow(irrefutable_let_patterns)]
            if let Control::Exit = msg {
                download_tx.send(download::DownloadSignal::Quit).await?;
                break;
            }
        }
        Ok(())
    };
    futures::try_join!(download_fut, sync_fut, channel_fut)?;
    Ok(())
}
