use std::thread;
use std::thread::JoinHandle;

use async_std::{
    channel::Sender,
    sync::{Arc, Mutex},
};
use gdk::ModifierType;
use glib::{Receiver, object::Cast};
use gtk::{self, prelude::*, WidgetExt};
use ignore_result::Ignore;

pub use error::Error;
use frontend_interface::Images;
use settings::Settings;

use crate::backend::ControlFlow;

mod backend;
mod common_structs;
mod current_downloads;
mod download_list;
mod error;
mod frontend_interface;
mod like_list;
mod localization;
mod offline_search;
mod progress_bar;
mod settings;
mod helper;

/// Widgets needed for the app to work
pub struct WindowObjects {
    pub window: gtk::ApplicationWindow,
    pub progress: progress_bar::Gui,
    pub download_list: download_list::frontend::DownloadFrontend,
    pub like_list: like_list::frontend::LikeFrontend,
    pub current_download_list: current_downloads::frontend::CurrentDownloadFrontend,
    pub stream_list: gtk::ListBox,
    pub offline_search: offline_search::frontend::OfflineSearchFrontend,
}

/// Build the UI and start both the foreground and background listeners
pub fn build_ui(
    application: &gtk::Application,
    sender_channels: SenderChannels,
    receiver_channels: ReceiverChannels,
    settings: Arc<Mutex<Settings>>,
) {
    let mut window_objects = frontend_interface::construct_window(settings.clone(), sender_channels.clone());

    let window = window_objects.window.clone();

    window.set_application(Some(application));

    let settings_ref = async_std::task::block_on(settings.lock());

    window.set_default_size(
        settings_ref.window.width,
        settings_ref.window.height,
    );

    drop(settings_ref);

    window.show_all();

    window_objects.download_list.connect_events().unwrap();
    window_objects.like_list.connect_events().unwrap();
    window_objects
        .current_download_list
        .connect_events()
        .unwrap();
    window_objects.offline_search.connect_events().unwrap();
    window_objects.progress.connect_events().unwrap();

    window_objects
        .download_list
        .attach_handler(receiver_channels.download_rx)
        .unwrap();
    window_objects
        .like_list
        .attach_handler(receiver_channels.like_rx)
        .unwrap();
    window_objects
        .current_download_list
        .attach_handler(receiver_channels.current_rx)
        .unwrap();
    window_objects
        .offline_search
        .attach_handler(receiver_channels.offline_search_rx)
        .unwrap();
    window_objects
        .progress
        .attach_handler(receiver_channels.progress_rx)
        .unwrap();

    window_objects.download_list.check().unwrap();
    window_objects.like_list.check().unwrap();
    window_objects.current_download_list.check().unwrap();
    window_objects.offline_search.check().unwrap();
    window_objects.progress.check().unwrap();

    let settings_ref = async_std::task::block_on(settings.lock());

    window.move_(
        settings_ref.window.x_pos,
        settings_ref.window.y_pos,
    );

    drop(settings_ref);

    let application_clone = application.clone();

    receiver_channels
        .error_reporting_rx
        .attach(None, move |msg| match msg {
            backend::BackendError::Error(err) => {
                let dialog = gtk::MessageDialogBuilder::new()
                    .application(&application_clone)
                    .title(&fl!("error-box-title"))
                    .buttons(gtk::ButtonsType::Ok)
                    .destroy_with_parent(true)
                    .message_type(gtk::MessageType::Error)
                    .text(&fl!("error-box-content", message = err))
                    .build();
                dialog.get_children()
                    .into_iter()
                    .map(|w| w.dynamic_cast::<gtk::Label>())
                    .for_each(|l| if let Ok(l) = l {l.set_selectable(true)});
                dialog.show_all();
                glib::Continue(true)
            }
        });

    window.connect_key_release_event(move |wnd, key| match key.get_keyval().to_unicode() {
        Some('q') | Some('Q') => {
            if key.get_state().intersects(ModifierType::CONTROL_MASK) {
                unsafe {
                    wnd.destroy();
                }
                gtk::Inhibit(true)
            } else {
                gtk::Inhibit(false)
            }
        }
        _ => gtk::Inhibit(false),
    });


    window.connect_destroy(move |window| {
        let flow = ControlFlow::Quit;
        sender_channels
            .download_tx
            .try_send(download_list::backend::DownloadListAction::ControlFlow(flow.clone()))
            .ignore();
        sender_channels
            .like_tx
            .try_send(like_list::backend::LikeListAction::ControlFlow(
                flow.clone(),
            ))
            .ignore();
        sender_channels
            .current_tx
            .try_send(current_downloads::backend::ListAction::ControlFlow(
                flow.clone(),
            ))
            .ignore();
        sender_channels
            .offline_search_tx
            .try_send(offline_search::backend::OfflineSearchAction::ControlFlow(flow))
            .ignore();
        for _ in 0..10 {
            sender_channels
                .backend_control_tx
                .try_send(helper::Control::Exit)
                .ignore();
        }
        let (x, y) = window.get_position();
        let (width, height) = window.get_size();
        let mut settings_ref = async_std::task::block_on(settings.lock());
        settings_ref.window.width = width;
        settings_ref.window.height = height;
        settings_ref.window.x_pos = x;
        settings_ref.window.y_pos = y;
        settings_ref.write().unwrap();
    });
}

pub struct ReceiverChannels {
    download_rx: Receiver<download_list::frontend::DownloadListChange>,
    like_rx: Receiver<like_list::frontend::LikeListChange>,
    current_rx: Receiver<current_downloads::frontend::ListChange>,
    offline_search_rx: Receiver<offline_search::frontend::ListChange>,
    error_reporting_rx: Receiver<backend::BackendError>,
    progress_rx: Receiver<progress_bar::Event>,
}

#[derive(Clone)]
pub struct SenderChannels {
    download_tx: Sender<download_list::backend::DownloadListAction>,
    like_tx: Sender<like_list::backend::LikeListAction>,
    current_tx: Sender<current_downloads::backend::ListAction>,
    offline_search_tx: Sender<offline_search::backend::OfflineSearchAction>,
    backend_control_tx: Sender<helper::Control>,
    backend_download_tx: Sender<helper::download::DownloadSignal>,
}

pub fn build_backend() -> (
    JoinHandle<()>,
    SenderChannels,
    ReceiverChannels,
    Arc<Mutex<Settings>>,
) {
    let settings = Arc::new(Mutex::new(Settings::new().unwrap_or_else(|error| {
        panic!(
            "{}",
            fl!("settings-not-loaded", error = format!("{:?}", error))
        )
    })));

    let (backend_download_tx, backend_download_rx) = async_std::channel::unbounded();
    let (progress_tx, progress_rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    let (download_rx, download_tx, download_list_comm) =
        download_list::backend::Communication::new(backend_download_tx.clone());
    let (like_rx, like_tx, like_list_comm) = like_list::backend::Communication::new();
    let (current_rx, current_tx, current_list_comm) =
        current_downloads::backend::Communication::new();
    let (offline_search_rx, offline_search_tx, offline_search_comm) =
        offline_search::backend::Communication::new(download_tx.clone(), like_tx.clone());
    let (error_reporting_tx, error_reporting_rx) =
        glib::MainContext::channel(glib::Priority::default());
    let (backend_control_tx, backend_control_rx) = async_std::channel::unbounded();

    let arguments = backend::BackendArguments {
        download_list_comm,
        like_list_comm,
        current_list_comm,
        download_tx: download_tx.clone(),
        current_tx: current_tx.clone(),
        like_tx: like_tx.clone(),
        offline_search_comm,
        error_reporting_tx,
        backend_download_tx: backend_download_tx.clone(),
        backend_download_rx,
        backend_control_rx,
        progress_tx,
        settings: settings.clone(),
    };

    let thread = thread::spawn(move || backend::sync_init(arguments));

    let sender_channels = SenderChannels {
        download_tx,
        like_tx,
        current_tx,
        offline_search_tx,
        backend_control_tx,
        backend_download_tx,
    };

    let receiver_channels = ReceiverChannels {
        download_rx,
        like_rx,
        current_rx,
        offline_search_rx,
        error_reporting_rx,
        progress_rx,
    };
    (thread, sender_channels, receiver_channels, settings)
}

#[cfg(test)]
mod tests {
    //#[test]
    //fn it_works() {
    //    assert_eq!(2 + 2, 4);
    //}
}
