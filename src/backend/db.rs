use std::process::Command;

use async_std::sync::{Arc, Mutex};
use sqlx::SqlitePool;

/// Initialize the database tables, if needed.
/// If it doesn't exist, the database file gets created first
/// Returns the database pool for later use
pub async fn init(settings: Arc<Mutex<crate::Settings>>) -> Result<SqlitePool, sqlx::Error> {
    let settings_ref = settings.lock().await;
    let path = settings_ref.database.path.clone();
    drop(settings_ref);
    // Create the database using the sqlite3 command, may not work on windows, idk
    if !std::path::Path::new(&path).exists() {
        Command::new("sqlite3")
            .arg(&path)
            .arg("CREATE TABLE tmp (id INTEGER PRIMARY KEY); DROP TABLE tmp;")
            .output()
            .expect("Failed to initialize db file");
    }

    let pool = sqlx::SqlitePool::connect(&path).await?;

    sqlx::query_file!("src/backend/queries/init.sql")
        .fetch_optional(&pool)
        .await?;

    Ok(pool)
}
