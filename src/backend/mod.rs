use std::error::Error;

use async_std::{
    channel::{Receiver, Sender},
    sync::{Arc, Mutex},
};
use ignore_result::Ignore;

use crate::current_downloads::backend as current_downloads;
use crate::download_list::backend as download;
use crate::helper::{self, download as backend_download};
use crate::like_list::backend as like;
use crate::offline_search::backend as offline_search;
use crate::progress_bar::Event as ProgressEvent;

pub mod db;

#[derive(Clone)]
pub enum ControlFlow {
    Quit,
}

pub enum BackendError {
    Error(String),
}

pub struct BackendArguments {
    pub download_list_comm: download::Communication,
    pub like_list_comm: like::Communication,
    pub current_list_comm: current_downloads::Communication,
    pub download_tx: Sender<download::DownloadListAction>,
    pub current_tx: Sender<current_downloads::ListAction>,
    pub like_tx: Sender<like::LikeListAction>,
    pub offline_search_comm: offline_search::Communication,
    pub backend_download_rx: Receiver<backend_download::DownloadSignal>,
    pub backend_download_tx: Sender<backend_download::DownloadSignal>,
    pub backend_control_rx: Receiver<helper::Control>,
    pub progress_tx: glib::Sender<ProgressEvent>,
    pub error_reporting_tx: glib::Sender<BackendError>,
    pub settings: Arc<Mutex<crate::Settings>>,
}

/// Initialize the database and lists
/// Run the lists afterwards
pub fn sync_init(arguments: BackendArguments) {
    let error_reporting_tx = arguments.error_reporting_tx.clone();
    if let Err(err) = init(arguments) {
        let message = if let Some(err) = err.downcast_ref::<sqlx::Error>() {
            if let Some(err) = err.as_database_error() {
                format!("{}, {:?}", err.message(), err.code())
            } else {
                format!("{}", *err)
            }
        } else {
            err.to_string()
        };
        error_reporting_tx
            .send(BackendError::Error(message))
            .ignore();
    };
}

/// Initialize the database and lists
/// Run the lists afterwards
#[tokio::main]
pub async fn init(arguments: BackendArguments) -> Result<(), Box<dyn Error>> {
    let pool = db::init(arguments.settings.clone()).await.unwrap();

    let download_list_init = download::init(arguments.download_list_comm.to_gui.clone(), &pool);
    let like_list_init = like::init(arguments.like_list_comm.to_gui.clone(), &pool);
    let current_list_init =
        current_downloads::init(arguments.current_list_comm.to_gui.clone(), &pool);
    futures::try_join!(download_list_init, like_list_init, current_list_init)?;

    let download_list_cont = download::continuous(
        arguments.download_list_comm,
        arguments.current_tx.clone(),
        pool.clone(),
    );
    let like_list_cont = like::continuous(arguments.like_list_comm, pool.clone());
    let current_list_cont = current_downloads::continuous(
        arguments.current_list_comm,
        arguments.download_tx.clone(),
        pool.clone(),
    );
    let offline_search_cont =
        offline_search::continuous(arguments.offline_search_comm, pool.clone());
    let backend_cont = helper::run(
        pool.clone(),
        arguments.backend_control_rx,
        arguments.backend_download_rx,
        arguments.backend_download_tx,
        arguments.current_tx,
        arguments.download_tx,
        arguments.like_tx,
        &arguments.progress_tx,
        &arguments.error_reporting_tx,
        arguments.settings,
    );
    futures::try_join!(
        download_list_cont,
        like_list_cont,
        current_list_cont,
        offline_search_cont,
        backend_cont
    )?;
    pool.close().await;
    Ok(())
}
