-- Tables
CREATE TABLE IF NOT EXISTS channels (
    cid TEXT PRIMARY KEY NOT NULL CHECK (cid LIKE '%:channel:%'),
    name TEXT NOT NULL DEFAULT '',
    description TEXT NOT NULL DEFAULT '',
    subscribed BOOL NOT NULL DEFAULT false,
    instant BOOL NOT NULL DEFAULT false
);
CREATE TABLE IF NOT EXISTS videos (
    vid TEXT PRIMARY KEY NOT NULL CHECK (vid LIKE '%:video:%'),
    url TEXT NOT NULL CHECK (url like 'https://%'),
    title TEXT NOT NULL DEFAULT '',
    description TEXT NOT NULL DEFAULT '',
    cid TEXT NOT NULL,
    published DATETIME NOT NULL DEFAULT (datetime('now','localtime')),
    likes INT NOT NULL,
    dislikes INT NOT NULL,
    views INT NOT NULL,
    duration INT NOT NULL,
    comments INT NOT NULL,
    thumbnail_url TEXT,
    download_status INT NOT NULL DEFAULT 3,
    mark_watched INT NOT NULL DEFAULT 2,
    like INT,
    location TEXT,
    FOREIGN KEY (cid)
        REFERENCES channels (cid)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
);
CREATE TABLE IF NOT EXISTS streams (
    sid TEXT PRIMARY KEY NOT NULL CHECK (sid LIKE '%:stream:%'),
    title TEXT NOT NULL DEFAULT '',
    start_time INT NOT NULL DEFAULT (datetime('now','localtime')),
    cid NOT NULL,
    ended BOOL NOT NULL DEFAULT false,
    description TEXT NOT NULL DEFAULT '',
    rating REAL NOT NULL,
    rating_count INT NOT NULL,
    views INT NOT NULL,
    FOREIGN KEY (cid)
        REFERENCES channels (cid)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
);
CREATE TABLE IF NOT EXISTS download_list (
    id INTEGER PRIMARY KEY ASC,
    vid TEXT NOT NULL UNIQUE,
    FOREIGN KEY (vid)
        REFERENCES videos (vid)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
);
-- indexes
CREATE INDEX IF NOT EXISTS channels_instant
ON channels (instant);
CREATE INDEX IF NOT EXISTS channels_subscribed
ON channels (subscribed);
CREATE UNIQUE INDEX IF NOT EXISTS download_list_vid
ON download_list (vid);
CREATE INDEX IF NOT EXISTS videos_download_status
ON videos (download_status);
CREATE INDEX IF NOT EXISTS videos_cid
ON videos (cid);
CREATE INDEX IF NOT EXISTS videos_like
ON videos (like);
-- views
DROP VIEW IF EXISTS download_like_list;
CREATE VIEW IF NOT EXISTS download_like_list AS
SELECT v.vid, v.title, c.name AS channel, v.download_status, v."like"
FROM videos v
INNER JOIN channels c ON v.cid = c.cid;
DROP VIEW IF EXISTS channel_list;
CREATE VIEW IF NOT EXISTS channel_list AS
SELECT
    cid AS vid,
    name AS title,
    substr(description, 1, instr(description, '\n')-1) AS channel,
    0 AS download_status,
    0 AS like
FROM channels;
DROP VIEW IF EXISTS fts_videos_view;
CREATE VIEW IF NOT EXISTS fts_videos_view AS
SELECT
    url || ' ' || title || ' ' || description AS document,
    vid
FROM videos;
DROP VIEW IF EXISTS fts_channels_view;
CREATE VIEW IF NOT EXISTS fts_channels_view AS
SELECT
    cid || ' ' || name || ' ' || description AS document,
    cid
FROM channels;
DROP VIEW IF EXISTS popup_content;
CREATE VIEW IF NOT EXISTS popup_content AS
SELECT v.vid, v.title, v.description, v.url, v.cid, v.location AS path,
c.subscribed, v.likes, v.views, v.comments, v.duration
FROM videos v
INNER JOIN channels c ON c.cid = v.cid;
-- Full text search
CREATE VIRTUAL TABLE IF NOT EXISTS fts_videos USING fts5(
    document, vid UNINDEXED, tokenize = "trigram"
);
CREATE VIRTUAL TABLE IF NOT EXISTS fts_channels USING fts5(
    document, cid UNINDEXED, tokenize = "trigram"
);
-- triggers
CREATE TRIGGER IF NOT EXISTS videos_ad AFTER DELETE ON videos BEGIN
    DELETE FROM fts_videos WHERE vid = old.vid;
END;
CREATE TRIGGER IF NOT EXISTS videos_ai AFTER INSERT ON videos BEGIN
    INSERT INTO fts_videos(document, vid) SELECT * FROM fts_videos_view WHERE vid = new.vid;
END;
CREATE TRIGGER IF NOT EXISTS videos_auf AFTER UPDATE OF url, name, description ON videos BEGIN
    UPDATE fts_videos
        SET document = view.document
        FROM (SELECT document FROM fts_videos_view WHERE vid = new.vid) AS view
        WHERE vid = new.vid;
END;
CREATE TRIGGER IF NOT EXISTS videos_aul AFTER UPDATE OF download_status ON videos WHEN old.download_status = 0 BEGIN
    DELETE FROM download_list WHERE vid = new.vid;
END;
CREATE TRIGGER IF NOT EXISTS channels_ad AFTER DELETE ON channels BEGIN
    DELETE FROM fts_channels WHERE cid = old.cid;
END;
CREATE TRIGGER IF NOT EXISTS channels_ai AFTER INSERT ON channels BEGIN
    INSERT INTO fts_channels(document, cid) SELECT * FROM fts_channels_view WHERE cid = new.cid;
END;
CREATE TRIGGER IF NOT EXISTS channels_au AFTER UPDATE OF name, description ON channels BEGIN
    UPDATE fts_channels
        SET document = view.document
        FROM (SELECT document FROM fts_channels_view WHERE cid = new.cid) AS view
        WHERE cid = new.cid;
END;
