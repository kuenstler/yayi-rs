use async_std::channel::Sender;
use gtk::{ButtonExt, ProgressBarExt, BoxExt, WidgetExt, LabelExt};
use ignore_result::Ignore;

use crate::helper::download::DownloadSignal;
use crate::common_structs::Status;

#[derive(Debug, Clone)]
pub struct Gui {
    pub to_download_backend: Sender<DownloadSignal>,
    pub progress_bar: gtk::ProgressBar,
    pub cancel_btn: gtk::Button,
    pub text: gtk::Label,
    progress_box: gtk::Box,
    pub status: Status,
}

pub enum Event {
    OnProgress(f64),
    SetName(String),
}

impl Gui {
    pub fn new(to_download_backend: Sender<DownloadSignal>) -> Self {
        let progress_box = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Horizontal)
            .build();
        let progress_box_box = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Vertical)
            .parent(&progress_box)
            .expand(true)
            .build();
        let text = gtk::LabelBuilder::new()
            .parent(&progress_box_box)
            .build();
        let progress_bar = gtk::ProgressBarBuilder::new()
            .name("ProgressBar")
            .parent(&progress_box_box)
            .expand(true)
            .build();
        let cancel_img = gtk::ImageBuilder::new().icon_name("dialog-close").build();
        let cancel_btn = gtk::ButtonBuilder::new()
            .name("ProgressBarCance")
            .image(&cancel_img)
            .parent(&progress_box)
            .build();
        Gui {
            progress_bar,
            cancel_btn,
            to_download_backend,
            progress_box,
            text,
            status: Status::default(),
        }
    }

    pub fn add_to_widget(&mut self, parent: &gtk::Box) -> Result<(), crate::Error> {
        if let Status::Constructed = self.status {
            parent.pack_start(&self.progress_box, false, true, 0);
            self.status = Status::Added;
            Ok(())
        } else {
            Err(crate::Error::WrongState(
                "ProgressBar",
                Status::Constructed,
                self.status,
            ))
        }
    }

    pub fn connect_events(&mut self) -> Result<(), crate::Error> {
        if let Status::Added = self.status {
            let to_download_backend_clone = self.to_download_backend.clone();

            self.cancel_btn.connect_clicked(move |_| {
                to_download_backend_clone
                    .try_send(DownloadSignal::CancelCurrent)
                    .ignore();
            });

            self.status = Status::Connected;
            Ok(())
        } else {
            Err(crate::Error::WrongState("ProgressBar", Status::Added, self.status))
        }
    }

    fn set_progress(&self, prog: f64) {
        if !self.progress_box.is_visible() {
            self.progress_box.show_all()
        }
        self.progress_bar.set_fraction(prog)
    }

    fn set_text(&self, text: String) {
        if !self.progress_box.is_visible() {
            self.progress_box.show_all()
        }
        self.text.set_text(&text)
    }

    pub fn handle_event(&self, event: Event) {
        match event {
            Event::OnProgress(prog) => self.set_progress(prog),
            Event::SetName(text) => self.set_text(text)
        }
    }

    pub fn attach_handler(
        &mut self,
        action_rx: glib::Receiver<Event>,
    ) -> Result<(), crate::Error> {
        if let Status::Connected = self.status {
            let self_clone = self.clone();

            action_rx.attach(None, move |msg| {
                self_clone.handle_event(msg);
                glib::Continue(true)
            });
            self.status = Status::Attached;
            Ok(())
        } else {
            Err(crate::Error::WrongState(
                "ProgressBar",
                Status::Connected,
                self.status,
            ))
        }
    }

    pub fn check(&self) -> Result<(), crate::Error> {
        if let Status::Attached = self.status {
            Ok(())
        } else {
            Err(crate::Error::WrongState(
                "ProgressBar",
                Status::Attached,
                self.status,
            ))
        }
    }
}
