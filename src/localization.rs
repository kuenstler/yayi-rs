use i18n_embed::{
    DesktopLanguageRequester,
    fluent::{fluent_language_loader, FluentLanguageLoader}, LanguageLoader, select,
};
use once_cell::sync::Lazy;
use rust_embed::RustEmbed;

/// Storage for localizations, filled during compile time and included in the binary
#[derive(RustEmbed)]
#[folder = "i18n/"]
pub struct Localizations;

/// The LanguageLoader for localization
pub static LANGUAGE_LOADER: Lazy<FluentLanguageLoader> = Lazy::new(|| {
    let loader: FluentLanguageLoader = fluent_language_loader!();

    // Load the fallback langauge by default so that users of the
    // library don't need to if they don't care about localization.
    loader
        .load_fallback_language(&Localizations)
        .expect("Error while loading fallback language");

    let requested_languages = DesktopLanguageRequester::requested_languages();
    let _result = select(&loader, &Localizations, &requested_languages);

    loader
});

/// shorthand for i18n_embed_fl::fl which requires a LanguageLoader as first argument
#[macro_export]
macro_rules! fl {
    ($message_id:literal) => {{
        i18n_embed_fl::fl!($crate::localization::LANGUAGE_LOADER, $message_id)
    }};

    ($message_id:literal, $($args:expr),*) => {{
        i18n_embed_fl::fl!($crate::localization::LANGUAGE_LOADER, $message_id, $($args), *)
    }};
}
