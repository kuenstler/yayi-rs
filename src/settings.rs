use std::{fs, path::Path};
use std::error::Error;

use config::{Config, ConfigError, Environment, File, FileFormat};
use dirs::{config_dir, data_dir};
use serde::{Deserialize, Serialize};
use toml::ser;

use crate::fl;

/// Name of the config file
const CONFIG_FILE: &str = "config.toml";

/// options for the database
#[derive(Debug, Deserialize, Serialize)]
pub struct Database {
    pub path: String,
}

/// Options for the main window
#[derive(Debug, Deserialize, Serialize)]
pub struct Window {
    pub width: i32,
    pub height: i32,
    pub x_pos: i32,
    pub y_pos: i32,
}

/// Settings
#[derive(Debug, Deserialize, Serialize)]
pub struct Settings {
    pub data_location: String,
    pub fetch_interval: u64,
    pub video_location: String,
    pub database: Database,
    pub window: Window,
    #[serde(skip_serializing)]
    pub config_file: String,
}

impl Settings {
    /// Initialize and return settings
    pub fn new() -> Result<Self, ConfigError> {
        let mut s = Config::new();

        // You may also programmatically change settings
        let data_path = data_dir()
            .unwrap_or_else(|| panic!("{}", fl!("data-directory-not-found")))
            .join(Path::new("yayi"));

        if !data_path.is_dir() {
            fs::create_dir_all(&data_path)
                .unwrap_or_else(|_| panic!("{}", fl!("not-create-data-directory")))
        }

        // Set data paths / database location
        s.set_default(
            "data_location",
            data_path.to_str()
                .unwrap_or_else(|| panic!("{}", fl!("not-write-data-directory"))),
        )?;
        s.set_default(
            "database.path",
            data_path.join(Path::new("storage.db")).to_str().unwrap(),
        )?;

        // set window options
        s.set_default("window.height", 400)?;
        s.set_default("window.width", 800)?;
        s.set_default("window.x_pos", -1)?;
        s.set_default("window.y_pos", -1)?;

        s.set_default("fetch_interval", 60 * 10)?;
        s.set_default("video_location", "~/Videos")?;

        let config_path = config_dir()
            .unwrap_or_else(|| panic!("{}", fl!("configuration-directory-not-found")))
            .join(Path::new("yayi"));

        if !config_path.is_dir() {
            fs::create_dir_all(&config_path)
                .unwrap_or_else(|_| panic!("{}", fl!("not-create-configuration-directory")))
        }

        // Add in a local configuration file
        // This file shouldn't be checked in to git
        let config_file = config_path.join(Path::new(CONFIG_FILE));
        s.set("config_file", config_file.to_str().unwrap())?;
        s
            .merge(
                File::new(config_file.to_str().unwrap(), FileFormat::Toml)
//                .required(false)
        )?
            .merge(
                Environment::default()
                   .prefix("APP")
                   .ignore_empty(false)
                   .separator(".")
        )?;

        // You can deserialize (and thus freeze) the entire configuration as
        s.try_into()
    }

    pub fn write(&self) -> Result<(), Box<dyn Error>> {
        let toml_content = ser::to_string(self)?;
        let config_file = Path::new(&self.config_file);
        fs::write(config_file, toml_content)?;
        Ok(())
    }
}
