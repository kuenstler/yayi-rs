/// Content for a download list popup
pub struct PopupUi {
    pub vid: String,
    pub title: String,
    pub description: String,
    pub url: String,
    pub cid: String,
    pub path: Option<String>,
    pub duration: i64,
    pub likes: i64,
    pub comments: i64,
    pub views: i64,
    pub subscribed: bool,
}

/// Content to add a new button to download_list
#[derive(Debug)]
pub struct AddButton {
    pub vid: String,
    pub title: String,
    pub channel: String,
    pub like: Option<i64>,
    pub download_status: i64,
}

#[derive(Debug)]
pub struct Videos {
    pub vid: String,
    pub url: String,
    pub title: String,
    pub description: String,
    pub cid: String,
    pub published: chrono::DateTime<chrono::Local>,
    pub likes: i64,
    pub dislikes: i64,
    pub views: i64,
    pub duration: i64,
    pub comments: i64,
    pub thumbnail_url: Option<String>,
    pub download_status: i64,
    pub mark_watched: i64,
    pub like: Option<i64>,
    pub location: Option<String>,
}

/// Initialisation status of the frontend
#[derive(Debug, Copy, Clone)]
pub enum Status {
    Constructed,
    Added,
    Connected,
    Attached,
}

impl ToString for Status {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}

impl Default for Status {
    fn default() -> Self {
        Self::Constructed
    }
}
