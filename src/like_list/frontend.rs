//! These methods should be executed within the main GUI thread and do no blocking work.
//! They are just spawning and handling windows and send the blocking work in form of
//! messages to the backend using appropriate channels

use gdk::ModifierType;
use gtk::prelude::*;
use ignore_result::Ignore;

use crate::common_structs::{AddButton, PopupUi, Status};
use crate::fl;
use crate::frontend_interface::framed_list;

use super::backend::LikeListAction;

/// Events for the download list
pub enum LikeListChange {
    AddButton(AddButton),
    RemoveButton(String),
    ConstructPopup(PopupUi),
}

#[derive(Clone)]
pub struct LikeFrontend {
    list: gtk::ListBox,
    frame: gtk::Frame,
    status: Status,
    to_backend: async_std::channel::Sender<LikeListAction>,
}

impl LikeFrontend {
    pub fn new(to_backend: async_std::channel::Sender<LikeListAction>) -> Self {
        // Download list
        let (frame, list) = framed_list("Like", &fl!("like-list-label"));
        LikeFrontend {
            list,
            frame,
            status: Default::default(),
            to_backend,
        }
    }

    pub fn add_to_widget<W: IsA<gtk::Container>>(
        &mut self,
        parent: &W,
    ) -> Result<(), crate::Error> {
        if let Status::Constructed = self.status {
            parent.add(&self.frame);
            self.status = Status::Added;
            Ok(())
        } else {
            Err(crate::Error::WrongState("LikesFrontend", Status::Constructed, self.status))
        }
    }

    /// Send a message if an entry exists, returns if the event should be handed over to the parent
    /// widget.
    fn emit_click_if_exists(
        lst: &gtk::ListBox,
        like_tx: &async_std::channel::Sender<LikeListAction>,
    ) -> bool {
        if let Some(child) = lst.get_selected_row() {
            let name = child.get_widget_name();
            let id = name[0..name.len() - 7].to_string();
            like_tx.try_send(LikeListAction::EntryClicked(id)).ignore();
            true
        } else {
            false
        }
    }

    /// Connect events for current_list
    pub fn connect_events(&mut self) -> Result<(), crate::Error> {
        if let Status::Added = self.status {
            let download_tx_clone = self.to_backend.clone();

            self.list.connect_button_press_event(move |lst, _| {
                let status = LikeFrontend::emit_click_if_exists(&lst, &download_tx_clone);
                gtk::Inhibit(status)
            });

            let download_tx_clone = self.to_backend.clone();

            self.list.connect_key_release_event(move |lst, event| {
                match event.get_keyval().to_unicode() {
                    Some('\r') => {
                        gtk::Inhibit(LikeFrontend::emit_click_if_exists(&lst, &download_tx_clone))
                    }
                    _ => gtk::Inhibit(false),
                }
            });
            self.status = Status::Connected;
            Ok(())
        } else {
            Err(crate::Error::WrongState("LikesFrontend", Status::Added, self.status))
        }
    }

    fn add_button(&self, add_btn: AddButton) {
        let title_label = gtk::LabelBuilder::new()
            .label(&fl!("like-list-title", title = add_btn.title))
            .tooltip_text(&fl!("like-title-tooltip"))
            .build();
        let channel_label = gtk::LabelBuilder::new()
            .label(&fl!("like-list-channel", channel = add_btn.channel))
            .tooltip_text(&fl!("like-channel-tooltip"))
            .build();
        let video_box = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Horizontal)
            .spacing(5)
            .name(&format!("{}LikeBox", add_btn.vid))
            .build();
        video_box.pack_start(&title_label, false, false, 0);
        video_box.pack_end(&channel_label, false, true, 0);
        let list_row = gtk::ListBoxRowBuilder::new()
            .name(&format!("{}LikeRow", add_btn.vid))
            .child(&video_box)
            .build();
        self.list.add(&list_row);
    }

    /// Send a given event to the backend and destroy the containing popup window
    fn send_and_destroy(
        &self,
        window: &gtk::Window,
        event: LikeListAction,
    ) -> impl Fn(&gtk::Button) {
        let window_clone = window.clone();
        let download_tx_clone = self.to_backend.clone();
        move |_| {
            download_tx_clone.try_send(event.clone()).ignore();
            unsafe {
                window_clone.destroy();
            }
        }
    }

    /// Construct a popup GUI and add callbacks to it
    /// Doesn't do any blocking work!
    pub fn construct_popup_gui(&self, data: PopupUi) {
        let images = crate::Images::new();
        let big_box = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        let window = gtk::WindowBuilder::new()
            .title(&fl!("like-video", video_name = data.title))
            .type_(gtk::WindowType::Toplevel)
            .child(&big_box)
            .decorated(true)
            .destroy_with_parent(true)
            .resizable(true)
            .window_position(gtk::WindowPosition::Mouse)
            .build();
        window.connect_key_release_event(|wnd, key| match key.get_keyval().to_unicode() {
            Some('\u{1b}') => {
                unsafe {
                    wnd.destroy();
                }
                gtk::Inhibit(true)
            }
            Some('q') | Some('Q') => {
                if key.get_state().intersects(ModifierType::CONTROL_MASK) {
                    unsafe {
                        wnd.destroy();
                    }
                    gtk::Inhibit(true)
                } else {
                    gtk::Inhibit(false)
                }
            }
            _ => gtk::Inhibit(false),
        });
        let like = gtk::ButtonBuilder::new()
            .label(&fl!("like-button"))
            .tooltip_text(&fl!("like-button-tooltip"))
            .build();
        {
            let vid_clone = data.vid.clone();
            like.connect_clicked(
                self.send_and_destroy(&window, LikeListAction::Liked(vid_clone, 1)),
            );
        }
        let not_like = gtk::ButtonBuilder::new()
            .label(&fl!("like-not"))
            .tooltip_text(&fl!("like-not-tooltip"))
            .build();
        {
            let vid_clone = data.vid.clone();
            not_like.connect_clicked(
                self.send_and_destroy(&window, LikeListAction::Liked(vid_clone, 0)),
            );
        }
        let dislike = gtk::ButtonBuilder::new()
            .label(&fl!("like-dislike"))
            .tooltip_text(&fl!("like-dislike-tooltip"))
            .build();
        {
            let vid_clone = data.vid.clone();
            dislike.connect_clicked(
                self.send_and_destroy(&window, LikeListAction::Liked(vid_clone, -1)),
            );
        }
        let subscribe = if data.subscribed {
            gtk::ButtonBuilder::new()
                .label(&fl!("like-unsubscribe-creator"))
                .build()
        } else {
            gtk::ButtonBuilder::new()
                .label(&fl!("like-subscribe-creator"))
                .build()
        };
        {
            let cid_clone = data.cid.clone();
            subscribe.connect_clicked(
                self.send_and_destroy(&window, LikeListAction::Subscribe(cid_clone)),
            );
        }
        let cancel = gtk::ButtonBuilder::new()
            .label(&fl!("like-cancel"))
            .image(&images.close)
            .build();
        {
            let window_clone = window.clone();
            cancel.connect_clicked(move |_| unsafe { window_clone.destroy() });
        }
        let view_browser = gtk::ButtonBuilder::new()
            .label(&fl!("like-view-in-browser"))
            .image(&images.browser)
            .build();
        {
            let url_clone = data.url.clone();
            view_browser.connect_clicked(
                self.send_and_destroy(&window, LikeListAction::ViewBrowser(url_clone)),
            );
        }
        let left = if let Some(_path) = data.path {
            let watch = gtk::ButtonBuilder::new()
                .label(&fl!("like-watch-video"))
                .image(&images.watch)
                .build();
            {
                let url_clone = data.url.clone();
                watch.connect_clicked(
                    self.send_and_destroy(&window, LikeListAction::ViewBrowser(url_clone)),
                );
            }
            let vbox = gtk::BoxBuilder::new()
                .orientation(gtk::Orientation::Vertical)
                .spacing(1)
                .build();
            vbox.pack_start(&like, false, true, 0);
            vbox.pack_start(&not_like, false, true, 0);
            vbox.pack_start(&dislike, false, true, 0);
            vbox.pack_start(&watch, false, true, 0);
            vbox.pack_start(&view_browser, false, true, 0);
            vbox.pack_start(&subscribe, false, true, 0);
            vbox.pack_start(&cancel, false, true, 0);
            vbox
        } else {
            let vbox = gtk::BoxBuilder::new()
                .orientation(gtk::Orientation::Vertical)
                .spacing(1)
                .build();
            vbox.pack_start(&like, false, true, 0);
            vbox.pack_start(&not_like, false, true, 0);
            vbox.pack_start(&dislike, false, true, 0);
            vbox.pack_start(&view_browser, false, true, 0);
            vbox.pack_start(&subscribe, false, true, 0);
            vbox.pack_start(&cancel, false, true, 0);
            vbox
        };
        let scrolled = gtk::ScrolledWindowBuilder::new()
            .width_request(400)
            .height_request(500)
            .build();
        let view = gtk::ViewportBuilder::new().parent(&scrolled).build();
        gtk::LabelBuilder::new()
            .label(&fl!(
                "like-description",
                description = data.description,
                likes = data.likes,
                views = data.views,
                duration = data.duration,
                comments = data.comments
            ))
            .max_width_chars(70)
            .wrap(true)
            .wrap_mode(pango::WrapMode::Word)
            .parent(&view)
            .build();
        big_box.pack_start(&left, false, true, 0);
        big_box.pack_start(&scrolled, false, true, 0);
        window.show_all();
    }

    /// handle a LikeListChange event
    pub fn handle_event(&self, event: LikeListChange) {
        match event {
            LikeListChange::AddButton(add_btn) => self.add_button(add_btn),
            LikeListChange::RemoveButton(vid) => {
                let name = format!("{}LikeRow", vid);
                self.list
                    .get_children()
                    .iter()
                    .filter(move |w| w.get_widget_name() == name)
                    .for_each(|w| self.list.remove(w));
            }
            LikeListChange::ConstructPopup(ui) => self.construct_popup_gui(ui),
        }
        self.list.show_all();
    }

    pub fn attach_handler(
        &mut self,
        action_rx: glib::Receiver<LikeListChange>,
    ) -> Result<(), crate::Error> {
        if let Status::Connected = self.status {
            let self_clone = self.clone();

            action_rx.attach(None, move |msg| {
                self_clone.handle_event(msg);
                glib::Continue(true)
            });
            self.status = Status::Attached;
            Ok(())
        } else {
            Err(crate::Error::WrongState("LikesFrontend", Status::Connected, self.status))
        }
    }

    pub fn check(&self) -> Result<(), crate::Error> {
        if let Status::Attached = self.status {
            Ok(())
        } else {
            Err(crate::Error::WrongState("LikesFrontend", Status::Attached, self.status))
        }
    }
}
