use std::error::Error;

use async_std::channel::{Receiver as AReceiver, Sender as ASender, unbounded};
use futures::StreamExt;
use sqlx::{self, SqlitePool};

use crate::backend::ControlFlow;
use crate::common_structs::{AddButton, PopupUi};

use super::frontend::*;

/// An event that gets emitted by a download list popup
#[derive(Clone)]
pub enum LikeListAction {
    // vid
    EntryClicked(String),
    // vid, like
    Liked(String, i64),
    // vid
    Subscribe(String),
    // cid
    ViewBrowser(String),
    // vid
    AddEntry(String),
    ControlFlow(ControlFlow),
}

/// Communication channels for continuous
pub struct Communication {
    pub to_gui: glib::Sender<LikeListChange>,
    pub from_gui: AReceiver<LikeListAction>,
}

impl Communication {
    pub fn new() -> (
        glib::Receiver<LikeListChange>,
        ASender<LikeListAction>,
        Communication,
    ) {
        let (list_change_tx, list_change_rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let (list_action_tx, list_action_rx) = unbounded();
        let communication = Communication {
            to_gui: list_change_tx,
            from_gui: list_action_rx,
        };
        (list_change_rx, list_action_tx, communication)
    }
}

/// Fill the download list with initial entries
pub async fn init(
    like_list: glib::Sender<LikeListChange>,
    pool: &SqlitePool,
) -> Result<(), Box<dyn Error>> {
    let results: Vec<AddButton> =
        sqlx::query_file_as!(AddButton, "src/like_list/queries/select_list_videos.sql")
            .fetch_all(pool)
            .await?;

    for result in results {
        like_list.send(LikeListChange::AddButton(result))?;
    }

    Ok(())
}

/// After a download list entry has been clicked, this method will query the download list popup
pub async fn entry_clicked(
    id: String,
    pool: &SqlitePool,
    to_gui: &glib::Sender<LikeListChange>,
) -> Result<(), Box<dyn Error>> {
    let mut transaction = pool.begin().await?;
    let mut result: PopupUi = sqlx::query_file_as!(
        PopupUi,
        "src/like_list/queries/select_popup_content.sql",
        id
    )
        .fetch_one(&mut transaction)
        .await?;

    if let Some(path) = &result.path {
        if !std::path::Path::new(path).exists() {
            sqlx::query_file!("src/like_list/queries/update_clear_location.sql", id)
                .fetch_one(&mut transaction)
                .await?;
            result.path = None;
            transaction.commit().await?;
        }
    }

    to_gui.send(LikeListChange::ConstructPopup(result))?;

    Ok(())
}

/// Toggle the subscribed status of a channel
pub async fn toggle_subscribed(id: String, pool: &SqlitePool) -> Result<(), Box<dyn Error>> {
    sqlx::query_file!("src/like_list/queries/update_toggle_subscribed.sql", id)
        .fetch_one(pool)
        .await?;

    Ok(())
}

/// Query a video for downloading. priority currently does nothing, fifo
pub async fn like(
    vid: String,
    like: i64,
    pool: &SqlitePool,
    to_gui: &glib::Sender<LikeListChange>,
) -> Result<(), Box<dyn Error>> {
    sqlx::query_file!("src/like_list/queries/update_set_liked.sql", like, vid)
        .fetch_optional(&pool.clone())
        .await?;
    to_gui.send(LikeListChange::RemoveButton(vid))?;

    Ok(())
}

/// Fill the download list with initial entries
pub async fn add_entry(
    vid: String,
    like_list: &glib::Sender<LikeListChange>,
    pool: &SqlitePool,
) -> Result<(), Box<dyn Error>> {
    let result: AddButton = sqlx::query_file_as!(
        AddButton,
        "src/like_list/queries/select_list_video_by_id.sql",
        vid
    )
        .fetch_one(pool)
        .await?;

    like_list.send(LikeListChange::AddButton(result))?;

    Ok(())
}

/// Pipe listener, delegates messages from the main pipe to matching methods
pub async fn continuous(
    mut communication: Communication,
    pool: SqlitePool,
) -> Result<(), Box<dyn Error>> {
    let to_gui = communication.to_gui.clone();
    while let Some(msg) = communication.from_gui.next().await {
        match msg {
            LikeListAction::EntryClicked(id) => {
                async_std::task::block_on(entry_clicked(id, &pool, &to_gui))?
            }
            LikeListAction::Subscribe(cid) => {
                async_std::task::block_on(toggle_subscribed(cid, &pool))?
            }
            LikeListAction::ViewBrowser(url) => {
                webbrowser::open(&url)?;
            }
            LikeListAction::Liked(vid, watch) => {
                async_std::task::block_on(like(vid, watch, &pool, &to_gui))?
            }
            LikeListAction::AddEntry(vid) => {
                async_std::task::block_on(add_entry(vid, &to_gui, &pool))?
            }
            LikeListAction::ControlFlow(flow) => match flow {
                ControlFlow::Quit => break,
            },
        }
    }
    Ok(())
}
