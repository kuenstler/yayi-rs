use std::error::Error;

use futures::{stream::FuturesOrdered, StreamExt};
use sqlx::SqlitePool;

use crate::backend::ControlFlow;
use crate::common_structs::{AddButton, PopupUi};
use crate::download_list::backend::DownloadListAction;
use crate::like_list::backend::LikeListAction;

use super::frontend::ListChange;

type DownloadTx = async_std::channel::Sender<DownloadListAction>;
type LikeTx = async_std::channel::Sender<LikeListAction>;

pub struct Communication {
    pub download_tx: DownloadTx,
    pub like_tx: LikeTx,
    pub to_gui: glib::Sender<ListChange>,
    pub from_gui: async_std::channel::Receiver<OfflineSearchAction>,
}

pub enum OfflineSearchAction {
    // vid
    Info(String),
    // vid
    Download(String),
    // vid
    Browser(String),
    // search_string, type
    Search(String, SearchType),
    ControlFlow(ControlFlow),
}

pub enum SearchType {
    Channel,
    Video,
}

impl Communication {
    pub fn new(
        download_tx: DownloadTx,
        like_tx: LikeTx,
    ) -> (
        glib::Receiver<ListChange>,
        async_std::channel::Sender<OfflineSearchAction>,
        Self,
    ) {
        let (list_change_tx, list_change_rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let (list_action_tx, list_action_rx) = async_std::channel::unbounded();
        let comm = Communication {
            download_tx,
            like_tx,
            to_gui: list_change_tx,
            from_gui: list_action_rx,
        };
        (list_change_rx, list_action_tx, comm)
    }
}

async fn download(
    vid: String,
    download_tx: &DownloadTx,
    like_tx: &LikeTx,
    pool: &SqlitePool,
) -> Result<(), Box<dyn Error>> {
    if vid.contains(":video:") {
        let mut transaction = pool.begin().await?;
        sqlx::query_file!("src/offline_search/queries/update_mark_download.sql", vid)
            .fetch_optional(&mut transaction)
            .await?;
        transaction.commit().await?;
        download_tx
            .send(DownloadListAction::AddEntry(vid.clone()))
            .await?;
        like_tx.send(LikeListAction::AddEntry(vid)).await?;
    }

    Ok(())
}

struct Vid {
    pub vid: Option<String>,
}

struct Cid {
    pub cid: Option<String>,
}

async fn search(
    search_term: String,
    search_type: SearchType,
    pool: &SqlitePool,
    results: &mut Vec<String>,
    to_gui: &glib::Sender<ListChange>,
) -> Result<(), Box<dyn Error>> {
    results.clear();
    let pool_clone = pool.clone();
    to_gui.send(ListChange::ClearList)?;
    match search_type {
        SearchType::Video => {
            let res: Vec<Vid> = sqlx::query_file_as!(
                Vid,
                "src/offline_search/queries/select_fts_video.sql",
                search_term
            )
                .fetch_all(&pool_clone)
                .await?;
            results.append(&mut res.into_iter().filter_map(|id| id.vid).collect());
        }
        SearchType::Channel => {
            let res: Vec<Cid> = sqlx::query_file_as!(
                Cid,
                "src/offline_search/queries/select_fts_channel.sql",
                search_term
            )
                .fetch_all(&pool_clone)
                .await?;
            results.append(&mut res.into_iter().filter_map(|id| id.cid).collect());
        }
    }

    let mut futures: FuturesOrdered<_> = results.iter().map(move |vid| {
        let pool_clone = pool.clone();
        async move {
            if vid.contains(":video:") {
                sqlx::query_file_as!(
                    AddButton,
                    "src/offline_search/queries/select_videos_list_entry.sql",
                    vid
                )
                    .fetch_one(&pool_clone)
                    .await
            } else {
                sqlx::query_file_as!(
                    AddButton,
                    "src/offline_search/queries/select_channels_list_entry.sql",
                    vid
                )
                    .fetch_one(&pool_clone)
                    .await
            }
        }
    }).collect();
    while let Some(fut) = futures.next().await {
        if let Ok(result) = fut {
            to_gui.send(ListChange::AddEntry(result))?
        }
    };

    Ok(())
}

async fn info(
    vid: String,
    to_gui: &glib::Sender<ListChange>,
    pool: &SqlitePool,
) -> Result<(), Box<dyn Error>> {
    if vid.contains(":video:") {
        let popup = sqlx::query_file_as!(
            PopupUi,
            "src/offline_search/queries/select_popup_content.sql",
            vid
        )
            .fetch_one(pool)
            .await?;
        to_gui.send(ListChange::ShowInfo(popup))?;
    }

    Ok(())
}

struct Url {
    pub url: String,
}

async fn browser(vid: String, pool: &SqlitePool) -> Result<(), Box<dyn Error>> {
    let url: Url =
        sqlx::query_file_as!(Url, "src/offline_search/queries/select_video_url.sql", vid)
            .fetch_one(pool)
            .await?;

    webbrowser::open(&url.url)?;

    Ok(())
}

pub async fn continuous(
    mut communication: Communication,
    pool: SqlitePool,
) -> Result<(), Box<dyn Error>> {
    let mut results: Vec<String> = vec![];
    let to_gui = communication.to_gui;
    let download_tx = communication.download_tx;
    let like_tx = communication.like_tx;
    while let Some(msg) = communication.from_gui.next().await {
        match msg {
            OfflineSearchAction::Browser(vid) => async_std::task::block_on(browser(vid, &pool))?,
            OfflineSearchAction::Info(vid) => async_std::task::block_on(info(vid, &to_gui, &pool))?,
            OfflineSearchAction::Download(vid) => {
                async_std::task::block_on(download(vid, &download_tx, &like_tx, &pool))?
            }
            OfflineSearchAction::Search(search_term, search_type) => async_std::task::block_on(
                search(search_term, search_type, &pool, &mut results, &to_gui),
            )?,
            OfflineSearchAction::ControlFlow(flow) => match flow {
                ControlFlow::Quit => break,
            },
        }
    }
    Ok(())
}
