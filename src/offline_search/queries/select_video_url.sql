SELECT
CASE
    WHEN query.vid LIKE '%:video:%' THEN (SELECT url FROM videos WHERE vid = query.vid)
    ELSE (SELECT 'https://youtube.com/channel?cid=' || cid FROM channels WHERE cid = query.vid)
END AS "url!: _"
FROM (SELECT ? AS vid) AS query