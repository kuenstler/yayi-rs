use gdk::ModifierType;
use gtk::prelude::*;
use ignore_result::Ignore;

use crate::common_structs::{AddButton, PopupUi, Status};
use crate::fl;
use crate::frontend_interface::framed_list;

use super::backend::{OfflineSearchAction, SearchType};

pub enum ListChange {
    AddEntry(AddButton),
    ClearList,
    ShowInfo(PopupUi),
}

#[derive(Clone)]
pub struct OfflineSearchFrontend {
    type_selector: gtk::ComboBoxText,
    search_bar: gtk::SearchEntry,
    result_list: gtk::ListBox,
    view_browser: gtk::Button,
    download: gtk::Button,
    info: gtk::Button,
    search_box: gtk::Box,
    status: Status,
    to_backend: async_std::channel::Sender<OfflineSearchAction>,
}

impl OfflineSearchFrontend {
    pub fn new(to_backend: async_std::channel::Sender<OfflineSearchAction>) -> Self {
        // Offline search
        let (list_frame, result_list) = framed_list(
            "OfflineSearchResults",
            &fl!("offline-search-results-list-label"),
        );
        let download_image = gtk::ImageBuilder::new().icon_name("download").build();
        let download = gtk::ButtonBuilder::new()
            .image(&download_image)
            .tooltip_text(&fl!("offline-search-download-tooltip"))
            .build();
        let browser_image = gtk::ImageBuilder::new().icon_name("text-html").build();
        let view_browser = gtk::ButtonBuilder::new()
            .image(&browser_image)
            .tooltip_text(&fl!("offline-search-browser-tooltip"))
            .build();
        let info_icon = gtk::ImageBuilder::new().icon_name("help-about").build();
        let info = gtk::ButtonBuilder::new()
            .image(&info_icon)
            .tooltip_text(&fl!("offline-search-info-tooltip"))
            .build();
        let button_box = gtk::ButtonBoxBuilder::new()
            .homogeneous(true)
            .orientation(gtk::Orientation::Vertical)
            .layout_style(gtk::ButtonBoxStyle::Expand)
            .build();
        button_box.add(&download);
        button_box.add(&view_browser);
        button_box.add(&info);
        let result_box = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Horizontal)
            .spacing(1)
            .build();
        result_box.pack_start(&list_frame, true, true, 0);
        result_box.pack_start(&button_box, false, false, 0);
        // TODO: add EntryCompletion
        let search_bar = gtk::SearchEntryBuilder::new()
            .name("OfflineSearchBar")
            .primary_icon_name("edit-find-symbolic")
            .secondary_icon_name("video-x-generic")
            .secondary_icon_activatable(false)
            .tooltip_text(&fl!("search-bar-tooltip"))
            .build();
        let type_selector = gtk::ComboBoxTextBuilder::new()
            .tooltip_text(&fl!("search-combo-tooltip"))
            .build();
        type_selector.append(Some("video"), &fl!("search-video"));
        type_selector.append(Some("channel"), &fl!("search-channel"));
        type_selector.set_active_id(Some("video"));
        let offline_search_bar_box = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Horizontal)
            .build();
        offline_search_bar_box.pack_start(&search_bar, true, true, 0);
        offline_search_bar_box.pack_start(&type_selector, false, false, 0);
        let search_box = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Vertical)
            .spacing(1)
            .name("OfflineSearchBox")
            .build();
        search_box.pack_start(&offline_search_bar_box, false, false, 0);
        search_box.pack_start(&result_box, true, true, 0);
        OfflineSearchFrontend {
            type_selector,
            search_bar,
            result_list,
            view_browser,
            download,
            info,
            search_box,
            status: Status::default(),
            to_backend,
        }
    }

    pub fn add_to_widget(&mut self, parent: &gtk::Notebook) -> Result<(), crate::Error> {
        if let Status::Constructed = self.status {
            let search_label = gtk::LabelBuilder::new()
                .name("OfflineSearchLabel")
                .label(&fl!("offline-search-tabs-label"))
                .tooltip_text(&fl!("offline-search-tabs-tooltip"))
                .build();
            parent.append_page(&self.search_box, Some(&search_label));
            self.status = Status::Added;
            Ok(())
        } else {
            Err(crate::Error::WrongState(
                "OfflineSearchFrontend",
                Status::Constructed,
                self.status,
            ))
        }
    }

    pub fn connect_events(&mut self) -> Result<(), crate::Error> {
        if let Status::Added = self.status {
            let to_backend_clone = self.to_backend.clone();
            let list_clone = self.result_list.clone();

            self.view_browser.connect_clicked(move |_| {
                if let Some(child) = list_clone.get_selected_row() {
                    let name = child.get_widget_name();
                    let id = name[0..name.len() - 16].to_string();
                    to_backend_clone
                        .try_send(OfflineSearchAction::Browser(id))
                        .ignore();
                }
            });

            let to_backend_clone = self.to_backend.clone();
            let list_clone = self.result_list.clone();

            self.download.connect_clicked(move |_| {
                if let Some(child) = list_clone.get_selected_row() {
                    let name = child.get_widget_name();
                    let id = name[0..name.len() - 16].to_string();
                    to_backend_clone
                        .try_send(OfflineSearchAction::Download(id))
                        .ignore();
                }
            });

            let to_backend_clone = self.to_backend.clone();
            let list_clone = self.result_list.clone();

            self.info.connect_clicked(move |_| {
                if let Some(child) = list_clone.get_selected_row() {
                    let name = child.get_widget_name();
                    let id = name[0..name.len() - 16].to_string();
                    to_backend_clone
                        .try_send(OfflineSearchAction::Info(id))
                        .ignore();
                }
            });

            let to_backend_clone = self.to_backend.clone();
            let combo_box_clone = self.type_selector.clone();
            let search_bar_clone = self.search_bar.clone();

            // maybe connect_activate
            self.search_bar.connect_search_changed(move |_| {
                if search_bar_clone.get_text() == "" {} else if let Some(combobox_id) = combo_box_clone.get_active_id() {
                    let search_type = match &combobox_id.to_string()[..] {
                        "channel" => SearchType::Channel,
                        _ => SearchType::Video,
                    };
                    to_backend_clone
                        .try_send(OfflineSearchAction::Search(
                            search_bar_clone.get_text().into(),
                            search_type,
                        ))
                        .ignore();
                }
            });

            let to_backend_clone = self.to_backend.clone();
            let combo_box_clone = self.type_selector.clone();
            let search_bar_clone = self.search_bar.clone();

            self.type_selector.connect_changed(move |_| {
                if search_bar_clone.get_text() == "" {} else if let Some(combobox_id) = combo_box_clone.get_active_id() {
                    let search_type = match &combobox_id.to_string()[..] {
                        "channel" => SearchType::Channel,
                        _ => SearchType::Video,
                    };
                    to_backend_clone
                        .try_send(OfflineSearchAction::Search(
                            search_bar_clone.get_text().into(),
                            search_type,
                        ))
                        .ignore();
                }
            });
            self.status = Status::Connected;
            Ok(())
        } else {
            Err(crate::Error::WrongState("OfflineSearchFrontend", Status::Added, self.status))
        }
    }

    fn add_entry(&self, btn: AddButton) {
        let title_label = gtk::LabelBuilder::new()
            .label(&fl!("offline-search-list-title", title = btn.title))
            .tooltip_text(&fl!("offline-search-title-tooltip"))
            .build();
        let channel_label = gtk::LabelBuilder::new()
            .label(&fl!("offline-search-list-channel", channel = btn.channel))
            .tooltip_text(&fl!("offline-search-channel-tooltip"))
            .build();
        let video_box = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Horizontal)
            .spacing(5)
            .name(&format!("{}OfflineSearchBox", btn.vid))
            .build();
        video_box.pack_start(&title_label, false, false, 0);
        video_box.pack_end(&channel_label, false, true, 0);
        let list_row = gtk::ListBoxRowBuilder::new()
            .name(&format!("{}OfflineSearchRow", btn.vid))
            .child(&video_box)
            .build();
        self.result_list.add(&list_row);
        self.result_list.show_all();
    }

    fn build_info_popup(data: PopupUi) {
        let window = gtk::WindowBuilder::new()
            .title(&fl!("offline-search-video-info", video_name = data.title))
            .type_(gtk::WindowType::Toplevel)
            .decorated(true)
            .destroy_with_parent(true)
            .resizable(true)
            .window_position(gtk::WindowPosition::Mouse)
            .build();
        window.connect_key_release_event(|wnd, key| match key.get_keyval().to_unicode() {
            Some('\u{1b}') => {
                unsafe {
                    wnd.destroy();
                }
                gtk::Inhibit(true)
            }
            Some('q') | Some('Q') => {
                if key.get_state().intersects(ModifierType::CONTROL_MASK) {
                    unsafe {
                        wnd.destroy();
                    }
                    gtk::Inhibit(true)
                } else {
                    gtk::Inhibit(false)
                }
            }
            _ => gtk::Inhibit(false),
        });
        let scrolled = gtk::ScrolledWindowBuilder::new()
            .width_request(400)
            .height_request(500)
            .parent(&window)
            .build();
        let view = gtk::ViewportBuilder::new().parent(&scrolled).build();
        gtk::LabelBuilder::new()
            .label(&fl!(
                "offline-search-description",
                description = data.description,
                likes = data.likes,
                views = data.views,
                duration = data.duration,
                comments = data.comments
            ))
            .max_width_chars(70)
            .wrap(true)
            .wrap_mode(pango::WrapMode::Word)
            .parent(&view)
            .build();
        window.show_all();
    }

    pub fn handle_event(&self, event: ListChange) {
        match event {
            ListChange::AddEntry(btn) => self.add_entry(btn),
            ListChange::ClearList => {
                while let Some(widget) = self.result_list.get_row_at_index(0) {
                    self.result_list.remove(&widget)
                }
            }
            ListChange::ShowInfo(data) => OfflineSearchFrontend::build_info_popup(data),
        }
    }

    pub fn attach_handler(
        &mut self,
        action_rx: glib::Receiver<ListChange>,
    ) -> Result<(), crate::Error> {
        if let Status::Connected = self.status {
            let self_clone = self.clone();

            action_rx.attach(None, move |msg| {
                self_clone.handle_event(msg);
                glib::Continue(true)
            });
            self.status = Status::Attached;
            Ok(())
        } else {
            Err(crate::Error::WrongState(
                "OfflineSearchFrontend",
                Status::Connected,
                self.status,
            ))
        }
    }

    pub fn check(&self) -> Result<(), crate::Error> {
        if let Status::Attached = self.status {
            Ok(())
        } else {
            Err(crate::Error::WrongState(
                "OfflineSearchFrontend",
                Status::Attached,
                self.status,
            ))
        }
    }
}
