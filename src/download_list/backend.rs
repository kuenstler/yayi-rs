use std::error::Error;

use async_std::channel::{Receiver as AReceiver, Sender as ASender, unbounded};
use futures::StreamExt;
use sqlx::{self, SqlitePool};

use crate::backend::ControlFlow;
use crate::common_structs::{AddButton, PopupUi};

use super::frontend::*;

type CurrentTx = async_std::channel::Sender<crate::current_downloads::backend::ListAction>;

/// An event that gets emitted by a download list popup
#[derive(Clone)]
pub enum DownloadListAction {
    // vid
    EntryClicked(String),
    // vid
    AddEntry(String),
    // vid, mark_watched, priority
    Download(String, bool, bool),
    // vid
    NoDownload(String),
    // url
    ViewBrowser(String),
    // cid
    Subscribe(String),
    ControlFlow(ControlFlow),
}

/// Communication channels for continuous
pub struct Communication {
    pub to_gui: glib::Sender<DownloadListChange>,
    pub from_gui: AReceiver<DownloadListAction>,
    pub to_back: ASender<crate::helper::download::DownloadSignal>,
}

impl Communication {
    pub fn new(to_back: ASender<crate::helper::download::DownloadSignal>,) -> (
        glib::Receiver<DownloadListChange>,
        ASender<DownloadListAction>,
        Communication,
    ) {
        let (list_change_tx, list_change_rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let (list_action_tx, list_action_rx) = unbounded();
        let communication = Communication {
            to_gui: list_change_tx,
            from_gui: list_action_rx,
            to_back,
        };
        (list_change_rx, list_action_tx, communication)
    }
}

/// Fill the download list with initial entries
pub async fn init(
    download_list: glib::Sender<DownloadListChange>,
    pool: &SqlitePool,
) -> Result<(), Box<dyn Error>> {
    let results: Vec<AddButton> = sqlx::query_file_as!(
        AddButton,
        "src/download_list/queries/select_list_videos.sql"
    )
        .fetch_all(pool)
        .await?;

    for result in results {
        download_list.send(DownloadListChange::AddButton(result))?;
    }

    Ok(())
}

/// After a download list entry has been clicked, this method will query the download list popup
pub async fn entry_clicked(
    id: String,
    pool: &SqlitePool,
    to_gui: &glib::Sender<DownloadListChange>,
) -> Result<(), Box<dyn Error>> {
    let mut transaction = pool.begin().await?;
    let mut result = sqlx::query_file_as!(
        PopupUi,
        "src/download_list/queries/select_popup_content.sql",
        id
    )
        .fetch_one(&mut transaction)
        .await?;

    if let Some(path) = &result.path {
        if !std::path::Path::new(path).exists() {
            sqlx::query_file!("src/download_list/queries/update_clear_location.sql", id)
                .fetch_optional(&mut transaction)
                .await?;
            result.path = None;
            transaction.commit().await?;
        }
    }

    to_gui.send(DownloadListChange::ConstructPopup(result))?;

    Ok(())
}

/// Toggle the subscribed status of a channel
pub async fn toggle_subscribed(id: String, pool: &SqlitePool) -> Result<(), Box<dyn Error>> {
    sqlx::query_file!("src/download_list/queries/update_toggle_subscribed.sql", id)
        .fetch_optional(pool)
        .await?;

    Ok(())
}

/// Do not download a vid but remove it from download list
pub async fn no_download(
    vid: String,
    pool: &SqlitePool,
    to_gui: &glib::Sender<DownloadListChange>,
) -> Result<(), Box<dyn Error>> {
    sqlx::query_file!("src/download_list/queries/update_no_download.sql", vid)
        .fetch_optional(pool)
        .await?;
    to_gui.send(DownloadListChange::RemoveButton(vid))?;

    Ok(())
}

/// Query a video for downloading. priority currently does nothing, fifo
pub async fn download(
    vid: String,
    watch: bool,
    _priority: bool,
    pool: &SqlitePool,
    to_gui: &glib::Sender<DownloadListChange>,
    current_tx: &CurrentTx,
    to_back: &ASender<crate::helper::download::DownloadSignal>,
) -> Result<(), Box<dyn Error>> {
    let watched: i32 = if watch { 1 } else { 0 };
    let mut transaction = pool.begin().await?;
    sqlx::query_file!(
        "src/download_list/queries/update_download.sql",
        watched,
        vid
    )
        .fetch_optional(&mut transaction)
        .await?;
    sqlx::query_file!("src/download_list/queries/insert_downloads_list.sql", vid)
        .fetch_optional(&mut transaction)
        .await?;
    transaction.commit().await?;
    to_gui.send(DownloadListChange::RemoveButton(vid.clone()))?;
    current_tx
        .send(crate::current_downloads::backend::ListAction::AddEntry(vid))
        .await?;
    to_back.send(crate::helper::download::DownloadSignal::Add)
        .await?;

    Ok(())
}

pub async fn add_entry(
    to_gui: &glib::Sender<DownloadListChange>,
    pool: &SqlitePool,
    vid: String,
) -> Result<(), Box<dyn Error>> {
    let result: AddButton = sqlx::query_file_as!(
        AddButton,
        "src/download_list/queries/select_list_video_by_id.sql",
        vid
    )
        .fetch_one(pool)
        .await?;

    to_gui.send(DownloadListChange::AddButton(result))?;

    Ok(())
}

/// Pipe listener, delegates messages from the main pipe to matching methods
pub async fn continuous(
    mut communication: Communication,
    current_tx: CurrentTx,
    pool: SqlitePool,
) -> Result<(), Box<dyn Error>> {
    let to_gui = communication.to_gui.clone();
    while let Some(msg) = communication.from_gui.next().await {
        match msg {
            DownloadListAction::EntryClicked(id) => {
                async_std::task::block_on(entry_clicked(id, &pool, &to_gui))?
            }
            DownloadListAction::Subscribe(cid) => {
                async_std::task::block_on(toggle_subscribed(cid, &pool))?
            }
            DownloadListAction::ViewBrowser(url) => {
                webbrowser::open(&url)?;
            }
            DownloadListAction::NoDownload(vid) => {
                async_std::task::block_on(no_download(vid, &pool, &to_gui))?
            }
            DownloadListAction::Download(vid, watch, priority) => async_std::task::block_on(
                download(vid, watch, priority, &pool, &to_gui, &current_tx, &communication.to_back),
            )?,
            DownloadListAction::AddEntry(vid) => {
                async_std::task::block_on(add_entry(&to_gui, &pool, vid))?
            }
            DownloadListAction::ControlFlow(flow) => match flow {
                ControlFlow::Quit => break,
            },
        }
    }
    Ok(())
}
