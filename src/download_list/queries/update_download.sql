UPDATE videos
    SET download_status = 0,
        mark_watched = ?
    WHERE vid = ?