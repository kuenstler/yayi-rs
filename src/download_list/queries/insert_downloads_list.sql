INSERT INTO download_list
    (id, vid) VALUES (IFNULL((
        SELECT id
            FROM download_list
            ORDER BY id DESC
            LIMIT 1
    ), 1) + 100, ?)