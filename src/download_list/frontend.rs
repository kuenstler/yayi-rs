//! These methods should be executed within the main GUI thread and do no blocking work.
//! They are just spawning and handling windows and send the blocking work in form of
//! messages to the backend using appropriate channels

use gdk::ModifierType;
use gtk::prelude::*;
use ignore_result::Ignore;

use crate::common_structs::{AddButton, PopupUi, Status};
use crate::fl;
use crate::frontend_interface::framed_list;

use super::backend::DownloadListAction;

/// Events for the download list
pub enum DownloadListChange {
    AddButton(AddButton),
    RemoveButton(String),
    ConstructPopup(PopupUi),
}

#[derive(Clone)]
pub struct DownloadFrontend {
    list: gtk::ListBox,
    frame: gtk::Frame,
    status: Status,
    to_backend: async_std::channel::Sender<DownloadListAction>,
}

impl DownloadFrontend {
    pub fn new(to_backend: async_std::channel::Sender<DownloadListAction>) -> Self {
        // Download list
        let (frame, list) = framed_list("Download", &fl!("download-list-label"));
        DownloadFrontend {
            list,
            frame,
            status: Default::default(),
            to_backend,
        }
    }

    pub fn add_to_widget<W: IsA<gtk::Container>>(
        &mut self,
        parent: &W,
    ) -> Result<(), crate::Error> {
        if let Status::Constructed = self.status {
            parent.add(&self.frame);
            self.status = Status::Added;
            Ok(())
        } else {
            Err(crate::Error::WrongState("DownloadsFrontend", Status::Constructed, self.status))
        }
    }

    /// Send a message if an entry exists, returns if the event should be handed over to the parent
    /// widget.
    fn emit_click_if_exists(
        lst: &gtk::ListBox,
        download_tx: &async_std::channel::Sender<DownloadListAction>,
    ) -> bool {
        if let Some(child) = lst.get_selected_row() {
            let name = child.get_widget_name();
            let id = name[0..name.len() - 11].to_string();
            download_tx
                .try_send(DownloadListAction::EntryClicked(id))
                .ignore();
            true
        } else {
            false
        }
    }

    /// Connect events for current_list
    pub fn connect_events(&mut self) -> Result<(), crate::Error> {
        if let Status::Added = self.status {
            let download_tx_clone = self.to_backend.clone();

            self.list.connect_button_press_event(move |lst, _| {
                let status = DownloadFrontend::emit_click_if_exists(&lst, &download_tx_clone);
                gtk::Inhibit(status)
            });

            let download_tx_clone = self.to_backend.clone();

            self.list.connect_key_release_event(move |lst, event| {
                match event.get_keyval().to_unicode() {
                    Some('\r') => gtk::Inhibit(DownloadFrontend::emit_click_if_exists(
                        &lst,
                        &download_tx_clone,
                    )),
                    _ => gtk::Inhibit(false),
                }
            });
            self.status = Status::Connected;
            Ok(())
        } else {
            Err(crate::Error::WrongState("DownloadsFrontend", Status::Added, self.status))
        }
    }

    fn add_button(&self, add_btn: AddButton) {
        let title_label = gtk::LabelBuilder::new()
            .label(&fl!("download-list-title", title = add_btn.title))
            .tooltip_text(&fl!("download-title-tooltip"))
            .build();
        let channel_label = gtk::LabelBuilder::new()
            .label(&fl!("download-list-channel", channel = add_btn.channel))
            .tooltip_text(&fl!("download-channel-tooltip"))
            .build();
        let video_box = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Horizontal)
            .spacing(5)
            .name(&format!("{}DownloadBox", add_btn.vid))
            .build();
        video_box.pack_start(&title_label, false, false, 0);
        video_box.pack_end(&channel_label, false, true, 0);
        let list_row = gtk::ListBoxRowBuilder::new()
            .name(&format!("{}DownloadRow", add_btn.vid))
            .child(&video_box)
            .build();
        self.list.add(&list_row);
    }

    /// Send a given event to the backend and destroy the containing popup window
    fn send_and_destroy(
        &self,
        window: &gtk::Window,
        event: DownloadListAction,
    ) -> impl Fn(&gtk::Button) {
        let window_clone = window.clone();
        let download_tx_clone = self.to_backend.clone();
        move |_| {
            download_tx_clone.try_send(event.clone()).ignore();
            unsafe {
                window_clone.destroy();
            }
        }
    }

    /// Construct a popup GUI and add callbacks to it
    /// Doesn't do any blocking work!
    fn construct_popup_gui(&self, data: PopupUi) {
        let images = crate::Images::new();
        let big_box = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        let window = gtk::WindowBuilder::new()
            .title(&fl!("download-video", video_name = data.title))
            .type_(gtk::WindowType::Toplevel)
            .child(&big_box)
            .decorated(true)
            .destroy_with_parent(true)
            .resizable(true)
            .window_position(gtk::WindowPosition::Mouse)
            .build();
        window.connect_key_release_event(|wnd, key| match key.get_keyval().to_unicode() {
            Some('\u{1b}') => {
                unsafe {
                    wnd.destroy();
                }
                gtk::Inhibit(true)
            }
            Some('q') | Some('Q') => {
                if key.get_state().intersects(ModifierType::CONTROL_MASK) {
                    unsafe {
                        wnd.destroy();
                    }
                    gtk::Inhibit(true)
                } else {
                    gtk::Inhibit(false)
                }
            }
            _ => gtk::Inhibit(false),
        });
        let subscribe = if data.subscribed {
            gtk::ButtonBuilder::new()
                .label(&fl!("download-unsubscribe-creator"))
                .build()
        } else {
            gtk::ButtonBuilder::new()
                .label(&fl!("download-subscribe-creator"))
                .build()
        };
        {
            let cid_clone = data.cid.clone();
            subscribe.connect_clicked(
                self.send_and_destroy(&window, DownloadListAction::Subscribe(cid_clone)),
            );
        }
        let cancel = gtk::ButtonBuilder::new()
            .label(&fl!("download-cancel"))
            .image(&images.close)
            .build();
        {
            let window_clone = window.clone();
            cancel.connect_clicked(move |_| unsafe { window_clone.destroy() });
        }
        let view_browser = gtk::ButtonBuilder::new()
            .label(&fl!("download-view-in-browser"))
            .image(&images.browser)
            .build();
        {
            let url_clone = data.url.clone();
            view_browser.connect_clicked(
                self.send_and_destroy(&window, DownloadListAction::ViewBrowser(url_clone)),
            );
        }
        let left = if let Some(_path) = data.path {
            let watch = gtk::ButtonBuilder::new()
                .label(&fl!("download-watch-video"))
                .image(&images.watch)
                .build();
            {
                let url_clone = data.url.clone();
                watch.connect_clicked(
                    self.send_and_destroy(&window, DownloadListAction::ViewBrowser(url_clone)),
                );
            }
            let vbox = gtk::BoxBuilder::new()
                .orientation(gtk::Orientation::Vertical)
                .spacing(1)
                .build();
            vbox.pack_start(&watch, false, true, 0);
            vbox.pack_start(&view_browser, false, true, 0);
            vbox.pack_start(&subscribe, false, true, 0);
            vbox.pack_start(&cancel, false, true, 0);
            vbox
        } else {
            let download_watch = gtk::ButtonBuilder::new()
                .label(&fl!("download-marking-watched"))
                .image(&images.download)
                .build();
            {
                let vid_clone = data.vid.clone();
                download_watch.connect_clicked(self.send_and_destroy(
                    &window,
                    DownloadListAction::Download(vid_clone, true, false),
                ));
            }
            let download_no_watch = gtk::ButtonBuilder::new()
                .label(&fl!("download-no-marking-watched"))
                .image(&images.download1)
                .build();
            {
                let vid_clone = data.vid.clone();
                download_no_watch.connect_clicked(self.send_and_destroy(
                    &window,
                    DownloadListAction::Download(vid_clone, false, false),
                ));
            }
            let no_download = gtk::ButtonBuilder::new()
                .label(&fl!("download-not"))
                .image(&images.not_download)
                .build();
            {
                let vid_clone = data.vid.clone();
                no_download.connect_clicked(
                    self.send_and_destroy(&window, DownloadListAction::NoDownload(vid_clone)),
                );
            }
            let download_prio = gtk::ButtonBuilder::new()
                .label(&fl!("download-prioritisation"))
                .image(&images.download_prio)
                .build();
            {
                let vid_clone = data.vid.clone();
                download_prio.connect_clicked(self.send_and_destroy(
                    &window,
                    DownloadListAction::Download(vid_clone, true, true),
                ));
            }
            let vbox = gtk::BoxBuilder::new()
                .orientation(gtk::Orientation::Vertical)
                .spacing(1)
                .build();
            vbox.pack_start(&download_watch, false, true, 0);
            vbox.pack_start(&download_no_watch, false, true, 0);
            vbox.pack_start(&no_download, false, true, 0);
            vbox.pack_start(&download_prio, false, true, 0);
            vbox.pack_start(&view_browser, false, true, 0);
            vbox.pack_start(&subscribe, false, true, 0);
            vbox.pack_start(&cancel, false, true, 0);
            vbox
        };
        let scrolled = gtk::ScrolledWindowBuilder::new()
            .width_request(400)
            .height_request(500)
            .build();
        let viewport = gtk::ViewportBuilder::new().parent(&scrolled).build();
        gtk::LabelBuilder::new()
            .label(&fl!(
                "download-description",
                description = data.description,
                likes = data.likes,
                views = data.views,
                duration = data.duration,
                comments = data.comments
            ))
            .max_width_chars(70)
            .wrap(true)
            .wrap_mode(pango::WrapMode::Word)
            .parent(&viewport)
            .build();
        big_box.pack_start(&left, false, true, 0);
        big_box.pack_start(&scrolled, false, true, 0);
        window.show_all();
    }

    /// handle a DownloadListChange event
    pub fn handle_event(&self, event: DownloadListChange) {
        match event {
            DownloadListChange::AddButton(add_btn) => self.add_button(add_btn),
            DownloadListChange::RemoveButton(vid) => {
                let name = format!("{}DownloadRow", vid);
                self.list
                    .get_children()
                    .iter()
                    .filter(move |w| w.get_widget_name() == name)
                    .for_each(|w| self.list.remove(w));
            }
            DownloadListChange::ConstructPopup(ui) => self.construct_popup_gui(ui),
        }
        self.list.show_all();
    }

    pub fn attach_handler(
        &mut self,
        action_rx: glib::Receiver<DownloadListChange>,
    ) -> Result<(), crate::Error> {
        if let Status::Connected = self.status {
            let self_clone = self.clone();

            action_rx.attach(None, move |msg| {
                self_clone.handle_event(msg);
                glib::Continue(true)
            });
            self.status = Status::Attached;
            Ok(())
        } else {
            Err(crate::Error::WrongState("DownloadsFrontend", Status::Connected, self.status))
        }
    }

    pub fn check(&self) -> Result<(), crate::Error> {
        if let Status::Attached = self.status {
            Ok(())
        } else {
            Err(crate::Error::WrongState("DownloadsFrontend", Status::Attached, self.status))
        }
    }
}
