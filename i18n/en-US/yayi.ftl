# kate: h1 Fluent

## Initial GUI
download-list-label = Download list
like-list-label = Like list
current-download-list-label = Current download list
stream-list-label = Stream list
offline-search-results-list-label = Offline search results
downloads-tabs-label = Downloads
downloads-tabs-tooltip = Current and pending downloads
streams-tabs-label = Streams
streams-tabs-tooltip = Running streams
offline-search-tabs-label = Offline search
offline-search-tabs-tooltip = Search for videos and channels in the local database
window-subtitle = Yet Another Youtube Interface

## Popup GUI Terms
-unsubscribe-creator = Unsubscribe creator
-subscribe-creator = Subscribe creator
-cancel = Cancel
-view-in-browser = View in browser
-watch-video = Watch video
-description =
    Likes: {$likes}
    Views: {$views}
    Duration: {$duration}
    Comments: {$comments}
    Description: {$description}
-title-tooltip = Title of the video
-channel-tooltip = Channel that published the video

## Download popup GUI
download-video = Download video {$video_name}
download-unsubscribe-creator = { -unsubscribe-creator }
download-subscribe-creator = { -subscribe-creator }
download-cancel = {-cancel}
download-watch-video = {-watch-video}
download-description = {-description(likes: $likes, views: $views, duration: $duration, comments: $comments, description: $description)}
download-view-in-browser = {-view-in-browser}
download-marking-watched = Download video while marking as watched
download-no-marking-watched = Download video without marking it as watched
download-not = Do not download the video
download-prioritisation = Download video with prioritisation
download-video = Download video {$video_name}
download-title-tooltip = {-title-tooltip}
download-channel-tooltip = {-channel-tooltip}

## Like popup gui
like-video = Like video {$video_name}
like-unsubscribe-creator = { -unsubscribe-creator }
like-subscribe-creator = { -subscribe-creator }
like-cancel = {-cancel}
like-watch-video = {-watch-video}
like-description = {-description(likes: $likes, views: $views, duration: $duration, comments: $comments, description: $description)}
like-view-in-browser = {-view-in-browser}
like-title-tooltip = {-title-tooltip}
like-channel-tooltip = {-channel-tooltip}
like-button = Like video
like-not = Do not like video
like-dislike = Dislike video
like-button-tooltip = Like the video
like-not-tooltip = Neither like nor dislike the video
like-dislike-tooltip = Dislike the video

## Current downloads
current-download-title-tooltip = {-title-tooltip}
current-download-up-tooltip = Move selected video up
current-download-down-tooltip = Move selected video down
current-download-top-tooltip = Move selected video to the top
current-download-bottom-tooltip = Move selected video to the bottom
current-download-cancel-tooltip = Cancel the download of the selected video

## Offline search
search-video = Video
search-channel = Channel
search-bar-tooltip = Search term
search-combo-tooltip = Type of content to search for
offline-search-download-tooltip = Add selected video to pending downloads
offline-search-browser-tooltip = View selected video in browser
offline-search-info-tooltip = Show information about selected video
offline-search-title-tooltip = {-title-tooltip}
offline-search-channel-tooltip = {-channel-tooltip}
offline-search-description = {-description(likes: $likes, views: $views, duration: $duration, comments: $comments, description: $description)}
offline-search-video-info = Information for {$video_name}

## Settings
settings-not-loaded = Settings couldn't be loaded due to {$error}
data-directory-not-found = Data directory not found
not-create-data-directory = Couldn't create data directory
not-write-data-directory = Couldn't write to data directory
not-create-configuration-directory = Couldn't create configuration directory
configuration-directory-not-found = Configuration directory not found

## List entries
-list-title = {$title}
-list-channel = {$channel}

download-list-title = {-list-title(title: $title)}
download-list-channel = {-list-channel(channel: $channel)}

like-list-title = {-list-title(title: $title)}
like-list-channel = {-list-channel(channel: $channel)}

current-download-list-title = {-list-title(title: $title)}

offline-search-list-title = {-list-title(title: $title)}
offline-search-list-channel = {-list-channel(channel: $channel)}

## Error box
error-box-title = Application error
error-box-content =
    The error '{$message}' occurred.
    Please report this on https://gitlab.com/kuenstler/yayi-rs/-/issues
    and describe what you have done before the error occured.

## Errors
internal-error-wrong-state = The internal component {$name} expected the internal state {$expected} but got {$received}
internal-error-youtube-missing = An Youtube component is missing: {$msg}
error-missing-media-group = The media:group component of Youtube feed
rustube-error = rustube encounterd the error {$err} on video {$vid}
no-video-stream-found = No stream found for video with vid: {$vid}
