## Initial GUI
download-list-label = Liste der herunterzuladenden Videos
like-list-label = Liste der zu likenden Videos
current-download-list-label = Download Warteschlange
stream-list-label = Liste der aktiven Streams
offline-search-results-list-label = Offline Suchergebnisse
downloads-tabs-label = Downloads
downloads-tabs-tooltip = Aktuelle und wartende Downloads
streams-tabs-label = Streams
streams-tabs-tooltip = Laufende Streams
offline-search-tabs-label = Offline suche
offline-search-tabs-tooltip = Suche nach Videos und Kanälen in der lokalen Datenbank
window-subtitle = Noch ein weiteres YouTube Interface

## Popup GUI Terms
-unsubscribe-creator = Ersteller deabonieren
-subscribe-creator = Ersteller abonnieren
-cancel = Abbrechen
-view-in-browser = Im Browser anzeigen
-watch-video = Video ansehen
-description =
    Likes: {$likes}
    Aufrufe: {$views}
    Dauer: {$duration}
    Kommentare: {$comments}
    Beschreibung: {$description}
-title-tooltip = Titel des videos
-channel-tooltip = Kanal, der das Video veröffentlicht hat

## Download popup GUI
download-marking-watched = Video herunterladen und als gesehen markieren
download-no-marking-watched = Video herunterladen und nicht als gesehen markieren
download-not = Video nicht herunterladen
download-prioritisation = Video mit Priorität herunterladen
download-video = {$video_name} herunterladen
download-unsubscribe-creator = { -unsubscribe-creator }
download-subscribe-creator = { -subscribe-creator }
download-cancel = {-cancel}
download-watch-video = {-watch-video}
download-description = {-description(likes: $likes, views: $views, duration: $duration, comments: $comments, description: $description)}
download-view-in-browser = {-view-in-browser}
download-title-tooltip = {-title-tooltip}
download-channel-tooltip = {-channel-tooltip}

## Like popup gui
like-video = Video {$video_name} liken
like-unsubscribe-creator = { -unsubscribe-creator }
like-subscribe-creator = { -subscribe-creator }
like-cancel = {-cancel}
like-watch-video = {-watch-video}
like-description = {-description(likes: $likes, views: $views, duration: $duration, comments: $comments, description: $description)}
like-view-in-browser = {-view-in-browser}
like-title-tooltip = {-title-tooltip}
like-channel-tooltip = {-channel-tooltip}
like-button = Mag ich
like-not = Neutral
like-dislike = Mag ich nicht
like-button-tooltip = Dem Video einen Daumen nach oben geben
like-not-tooltip = Dem Video weder einen Daumen nach oben noch nach unten geben
like-dislike-tooltip = Dem Video einen Daumen nach unten geben

## Current downloads
current-download-title-tooltip = {-title-tooltip}
current-download-up-tooltip = Ausgewähltes Video nach oben verschieben
current-download-down-tooltip = Ausgewähltes Video nach unten verschieben
current-download-top-tooltip = Ausgewähltes Video an die Spitze verschieben
current-download-bottom-tooltip = Ausgewähltes Video an das Ende verschieben
current-download-cancel-tooltip = Herunterladen des ausgewählten Videos abbrechen

## Offline search
search-video = Video
search-channel = Kanal
search-bar-tooltip = Suchbegriff
search-combo-tooltip = Art des Inhalts, nach dem gesucht werden soll
offline-search-download-tooltip = Füge das ausgewählte Video zu den wartenden Videos hinzu
offline-search-browser-tooltip = Zeige das ausgewählte Video im Browser
offline-search-info-tooltip = Zeige Informationen über ausgewähltes Video
offline-search-title-tooltip = {-title-tooltip}
offline-search-channel-tooltip = {-channel-tooltip}
offline-search-description = {-description(likes: $likes, views: $views, duration: $duration, comments: $comments, description: $description)}
offline-search-video-info = Informationen zu {$video_name}

## Settings
settings-not-loaded = Die Einstellungen konnten wegen {$error} nicht geladen werden
data-directory-not-found = Datenverzeichnis nicht gefunden
not-create-data-directory = Datenverzeichnis konnte nicht erstellt werden
not-write-data-directory = In Datenverzeichnis konnte nicht geschrieben werden
not-create-configuration-directory = Konfigurationsverzeichnis konnte nicht erstellt werden
configuration-directory-not-found = Konfigurationsverzeichnis nicht gefunden


## List entries
-list-title = {$title}
-list-channel = {$channel}

download-list-title = {-list-title(title: $title)}
download-list-channel = {-list-channel(channel: $channel)}

like-list-title = {-list-title(title: $title)}
like-list-channel = {-list-channel(channel: $channel)}

current-download-list-title = {-list-title(title: $title)}

offline-search-list-title = {-list-title(title: $title)}
offline-search-list-channel = {-list-channel(channel: $channel)}

## Error box
error-box-title = Anwendungsfehler
error-box-content =
    Der Fehler {$message} ist aufgetreten.
    Bitte melden Sie den Fehler unter https://gitlab.com/kuenstler/yayi-rs/-/issues
    und beschreiben Sie, was sie getan haben bevor der Fehler passiert ist.

## Errors
internal-error-wrong-state = Die interne Komponente {$name} hat den internen Status {$expected} erwartet, aber {$received} bekommen
